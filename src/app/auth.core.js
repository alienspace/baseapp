import React, { Component } from 'react';
import firebase from 'firebase';
import {
  Col, Button,
} from 'react-bootstrap';
import { browserHistory, } from 'react-router';
import { APP_CONFIG } from './boot.config';
// import { TOOLS } from './util/tools.global';
import { nextStorage } from './util/nextStorage';

/** COMPONENTS CUSTOM **/
import { NavHeader } from './components/Global/NavHeader/NavHeader';
export class Auth extends Component{
  constructor(props){
    super(props);
    this.state = {
      currentScreen : 'login',
      status : null,
      message : null,
      modeForm : this.props.modeForm
    }

    this.onPress = this.onPress.bind(this);
    this.signIn = this.signIn.bind(this);
    this.register = this.register.bind(this);
    this.signInWithGoogle = this.signInWithGoogle.bind(this);
    this.signOut = this.signOut.bind(this);
    this.logoutButton = this.logoutButton.bind(this);
    this.setMode = this.setMode.bind(this);
  };//constructor();

  signIn(e) {
    e.preventDefault();
    let _self=this, nextDash = nextStorage.getJSON('nextDash');
    _self.email = this.refs.email.value;
    _self.password = this.refs.password.value;

    if (!_self.email || !_self.password) {
      return _self.setState({ status : false, message : 'Preencha os campos email e senha.' });
    }

    // Sign in user
    firebase.auth().signInWithEmailAndPassword(_self.email, _self.password)
      .then(function(user) {
        if(user.uid)
          console.log(user.uid);
          window.sessionStorage.setItem('SESSION_id',nextDash.CART_idOrder);
          window.sessionStorage.setItem('userSession',JSON.stringify(user));
          _self.setState({ status : true, message : 'Bem-Vindo :D', isVisitor: false });
      })
      .catch(function(error) {
        // Handle Errors here.
        let errorCode = error.code;
        let errorMessage = error.message;
        if(errorCode === 'auth/invalid-email')
          errorMessage = 'E-mail inválido.';
        if(errorCode === 'auth/wrong-password')
          errorMessage = 'Senha inválida.';
        if(errorCode === 'auth/user-not-found')
          errorMessage = 'Registro não encontrado. Verifique os dados e tente novamente.';

        _self.setState({ status : false, message : errorMessage });
        console.log('signIn error', error);
      });
  };//signIn();

  register(e) {
    e.preventDefault();
    let _self=this;
    let email = this.refs.email.value;
    let password = this.refs.password.value;
    if (!email || !password) {
      return _self.setState({ status : false, message : 'Preencha os campos email e senha.' });
    }

    // Register user
    firebase.auth().createUserWithEmailAndPassword(email, password)
      .then((success) => {
        console.log('success createUserWithEmailAndPassword ',firebase.auth().UserInfo);
        const dataInsert = {
          uid : 'teste',
          email: email,
          password: password
        }
        firebase.database().ref('apps/'+ this.state.appID +'/users/'+this.state.uid)
          .set(dataInsert, () => {
            this.setState({
              isLoading : false,
            });
            return browserHistory.push('/dashboard/profile');
          })
      })
      .catch(function(error) {
        console.log('register error ',error);
        // Handle Errors here.
        let errorCode = error.code;
        let errorMessage = error.message;

        if(errorCode === 'auth/invalid-email')
          errorMessage = 'E-mail inválido.';
        if(errorCode === 'auth/weak-password')
          errorMessage = 'Senha curta demais. Necessário minímo 6 caracteres.';

        if (errorCode === 'auth/email-already-in-use') {
            errorMessage = 'E-mail já cadastrado.';

          // let credential = firebase.auth.EmailAuthProvider.credential(email, password);
          //
          // firebase.auth().signInWithGoogle()
          //   .then(function() {
          //     firebase.auth().currentUser.link(credential)
          //       .then(function(user) {
          //         console.log("Account linking success", user);
          //       }, function(error) {
          //         console.log("Account linking error", error);
          //       });
          //   });
        }

        _self.setState({ status : false, message : errorMessage });
      });
  };//register();

  signInWithGoogle() {
    // Sign in with Google
    let provider = new firebase.auth.GoogleAuthProvider();
    provider.addScope('profile');
    provider.addScope('email');

    return firebase.auth().signInWithPopup(provider)
      .catch(function(error) {
        console.log('Google sign in error', error);
      });
  };//signInWithGoogle();

  signOut(e) {
    e.preventDefault();
    firebase.auth().signOut();
    return location.href = '/';
  };//signOut

  onPress(e){
    if(e.keyCode === 13){
      e.preventDefault();
      this.signIn();
    }
  };//onPress();

  logoutButton(){
    if( this.props.currentUser && this.props.currentUser.email !== APP_CONFIG.MAIL_AUTH_VISITOR )
      return (<Button className={'btn btn-xs btn-danger'} onClick={this.signOut}><span>Sair</span></Button>);
  };//logoutButton();

  setMode(){
    if( this.state.modeForm === 'horizontal' ){
      if( !this.props.hoverLogin ){
        return (
          <form onSubmit={this.signIn} name={'login'} className={'text-left'} style={{marginTop:12}}>
            <Col md={3} className={'col-md-offset-2'}>
              <div className="input-group">
                <span className="input-group-addon no-radius" id="basic-addon1">E-mail</span>
                <input
                  className={'form-control no-radius'}
                  type="email"
                  ref="email"
                  value={this.state.email}
                  placeholder="" />
              </div>
            </Col>
            <Col md={3} className={'no-padding'}>
              <div className="input-group">
                <span className="input-group-addon no-radius" id="basic-addon1">Password</span>
                <input
                  className={'form-control no-radius'}
                  type="password"
                  ref="password" />
              </div>
            </Col>
            <Col md={1} className={'no-padding'}>
              <Button type="submit" className={'btn btn-md btn-success no-radius'} onClick={this.signIn}><span>Entrar</span></Button>
            </Col>
            <Col md={3} style={{marginTop:7}} className={'text-center no-padding'}>
              <Button className={'btn btn-xs btn-primary no-radius'} onClick={this.register}><span>Registrar-se</span></Button>
              <Button className={'btn btn-xs btn-primary no-radius'} onClick={this.signInWithGoogle}><span>Entrar com google</span></Button>
              {this.logoutButton()}
            </Col>
          </form>
        );
      }else{
        return (
          <form onSubmit={this.signIn} name={'login'} className={'text-left'} style={{marginTop:12}}>
            <Col md={12} className={'no-padding'}>
              <div className="input-group">
                <span className="input-group-addon no-radius" id="basic-addon1">E-mail</span>
                <input
                  className={'form-control no-radius'}
                  type="email"
                  ref="email"
                  value={this.state.email}
                  placeholder="" />
              </div>
            </Col>
            <Col md={9} className={'no-padding'}>
              <div className="input-group">
                <span className="input-group-addon no-radius" id="basic-addon1">Password</span>
                <input
                  className={'form-control no-radius'}
                  type="password"
                  ref="password" />
              </div>
            </Col>
            <Col md={2} className={'no-padding'}>
              <Button type="submit" className={'btn btn-md btn-success no-radius'} onClick={this.signIn}><span>Entrar</span></Button>
            </Col>
            <Col md={12} className={'text-center no-padding'}>
              <Button className={'btn btn-xs btn-primary no-radius'} onClick={this.register}><span>Registrar-se</span></Button>
              <Button className={'btn btn-xs btn-primary no-radius'} onClick={this.signInWithGoogle}><span>Entrar com google</span></Button>
              {this.logoutButton()}
            </Col>
          </form>
        );
      }
    }else if( this.state.modeForm === 'vertical' ){
      if( !this.props.shopLogin ){
        return (
          <Col style={{marginTop:150}}>
            <div className="panel panel-info">
              <div className="panel-heading">
                <h3 className="panel-title text-center">{ this.props.currentUser ? 'Dashboard CMS' : 'Acesso não autorizado!' }</h3>
              </div>
              <div className="panel-body">
                {this.state.message ?
                  <span className={'label label-warning'} style={{display:'block'}}>{this.state.message}</span>
                :
                  ''
                }
                {(this.state.currentScreen === 'register') ?
                  <form name={'login register'} className={'text-left'}>
                    <Col xs={12} md={12} className={'input-group'}>
                      <label>E-mail</label>
                      <input
                        className={'form-control no-radius'}
                        type="email"
                        ref="email"
                        autoFocus
                        value={this.state.email}
                        placeholder="" />
                    </Col>
                    <Col xs={12} md={12} className={'input-group no-padding'}>
                      <label>Senha</label>
                      <input
                        className={'form-control no-radius'}
                        type="password"
                        ref="password" />
                    </Col>
                    <Col xs={12} md={12} className={'input-group no-padding'}>
                      <label>Confirme sua senha</label>
                      <input
                        className={'form-control no-radius'}
                        type="password"
                        ref="password_again" />
                    </Col>
                    <Col xs={12} md={12} style={{marginTop:7}} className={'text-center no-padding'}>
                      <Button className={'btn btn-md btn-danger no-radius no-margin pull-left'} onClick={() => {this.setState({currentScreen:'login'})}}><span>Cancelar</span></Button>
                      <Button className={'btn btn-md btn-success no-radius pull-right'} style={{margin:'3px 0 0'}} onClick={this.register}><span>Registrar-se</span></Button>
                    </Col>
                  </form>
                :
                  <form onSubmit={this.signIn} name={'login'} className={'text-left'}>
                    <Col xs={12} md={12} className={'no-padding'}>
                      <div className="input-group">
                        <span className="input-group-addon no-radius" id="basic-addon1">E-mail</span>
                        <input
                          className={'form-control no-radius'}
                          type="email"
                          ref="email"
                          autoFocus
                          value={this.state.email}
                          placeholder="" />
                      </div>
                    </Col>
                    <Col xs={12} md={12} className={'no-padding'}>
                      <div className="input-group" style={{marginTop:5}}>
                        <span className="input-group-addon no-radius" id="basic-addon1">Password</span>
                        <input
                          className={'form-control no-radius'}
                          type="password"
                          ref="password" />
                      </div>
                    </Col>
                    <Col xs={12} md={12} style={{marginTop:7}} className={'text-center no-padding'}>
                      <Button type="submit" className={'btn btn-md no-radius btn-success no-margin pull-right'} onClick={this.signIn}><span>Entrar</span></Button>
                      <Button className={'btn btn-xs no-radius btn-info pull-left'} style={{margin:'3px 0 0'}} onClick={() => {this.setState({currentScreen:'register'})}}><span>Registrar-se</span></Button>
                    </Col>
                  </form>
                }
              </div>
              <span className={'label label-info pull-left rotateLeft90 no-radius'}>Version v{APP_CONFIG.PROJECT_VERSION}</span>
            </div>
          </Col>
        );
      }else{
        return (
          <Col>
            <div className="panel no-marginBottom">
              <div className="panel-body">
                {this.state.message ?
                  <span className={'label label-warning'} style={{display:'block'}}>{this.state.message}</span>
                :
                  ''
                }
                {(this.state.currentScreen === 'register') ?
                  <form name={'login register'} className={'text-left'}>
                    <Col xs={12} md={12} className={'input-group'}>
                      <label>E-mail</label>
                      <input
                        className={'form-control no-radius'}
                        type="email"
                        ref="email"
                        autoFocus
                        value={this.state.email}
                        placeholder="" />
                    </Col>
                    <Col xs={12} md={12} className={'input-group no-padding'}>
                      <label>Senha</label>
                      <input
                        className={'form-control no-radius'}
                        type="password"
                        ref="password" />
                    </Col>
                    <Col xs={12} md={12} className={'input-group no-padding'}>
                      <label>Confirme sua senha</label>
                      <input
                        className={'form-control no-radius'}
                        type="password"
                        ref="password_again" />
                    </Col>
                    <Col xs={12} md={12} style={{marginTop:7}} className={'text-center no-padding'}>
                      <Button className={'btn btn-md btn-danger no-radius no-margin pull-left'} onClick={() => {this.setState({currentScreen:'login'})}}><span>Cancelar</span></Button>
                      <Button className={'btn btn-md btn-success no-radius pull-right'} style={{margin:'3px 0 0'}} onClick={this.register}><span>Registrar-se</span></Button>
                    </Col>
                  </form>
                :
                  <form onSubmit={this.signIn} name={'login'} className={'text-left'}>
                    <Col xs={12} md={12} className={'no-padding'}>
                      <div className="input-group">
                        <span className="input-group-addon no-radius" id="basic-addon1">E-mail</span>
                        <input
                          className={'form-control no-radius'}
                          type="email"
                          ref="email"
                          autoFocus
                          value={this.state.email}
                          placeholder="" />
                      </div>
                    </Col>
                    <Col xs={12} md={12} className={'no-padding'}>
                      <div className="input-group" style={{marginTop:5}}>
                        <span className="input-group-addon no-radius" id="basic-addon1">Password</span>
                        <input
                          className={'form-control no-radius'}
                          type="password"
                          ref="password" />
                      </div>
                    </Col>
                    <Col xs={12} md={12} style={{marginTop:7}} className={'text-center no-padding'}>
                      <Button type="submit" className={'btn btn-md no-radius btn-success no-margin pull-right'} onClick={this.signIn}><span>Entrar</span></Button>
                      <Button className={'btn btn-xs no-radius btn-info pull-left'} style={{margin:'3px 0 0'}} onClick={() => {this.setState({currentScreen:'register'})}}><span>Registrar-se</span></Button>
                    </Col>
                  </form>
                }
              </div>
            </div>
          </Col>
        );
      }
    }
  };//setMode();

  render(){
    if( !this.props.shopLogin ){
      const xs = (this.state.modeForm === 'vertical') ? 12 : 12
      const md = (this.state.modeForm === 'vertical') ? 4 : 12
      const className = (this.state.modeForm === 'vertical') ? 'col-md-offset-4' : ''
      return (
        <Col xs={xs} md={md} className={className}>
        { this.props.currentUser && !this.props.isVisitor && !this.props.hoverLogin ?
            <NavHeader />
          :
          this.setMode()
        }
        </Col>

      );
    }else{
      const xs = (this.state.modeForm === 'vertical') ? 12 : 12
      const md = (this.state.modeForm === 'vertical') ? 12 : 12
      const className = (this.state.modeForm === 'vertical') ? '' : ''
      return (
        <Col xs={xs} md={md} className={className}>
        { this.props.currentUser && !this.props.isVisitor && !this.props.hoverLogin ?
            <NavHeader />
          :
          this.setMode()
        }
        </Col>

      );
    }
  }
}
