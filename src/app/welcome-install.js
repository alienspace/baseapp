const
  path = require('path'),
  fs = require('fs'),
  readline = require('readline')

console.log('\n\n\n');
console.log('             #####################################################');
console.log('             #####################################################');
console.log('             #####    DashSpace INSTALADO COM SUCESSO! :D    #####');
console.log('             ##################### OBRIGADO! #####################');
console.log('             #####################################################');
console.log('             #####  dashspace-start - http://localhost:3000  #####');
console.log('             #####################################################');
console.log('             #####################################################');
console.log('\n\n\n');

const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout
});

let dataDash = {};
rl.question('Project Name: ', (projectName) => {
  dataDash.projectName = projectName;
  rl.question(`${dataDash.projectName} - Project Slogan: `, (projectSlogan) => {

    if(projectSlogan === '')
      projectSlogan = 'Slogan não definido';
    dataDash.projectSlogan = projectSlogan;

    rl.question(`${dataDash.projectName} - Project Domain (not 'http://' or 'https://'): `, (projectDomain) => {
      if(projectDomain === '')
        projectDomain = 'domain.com';

      dataDash.projectDomain = projectDomain;

      if(dataDash.projectDomain.split('.')[0] === 'www'){
        dataDash.projectDomain = dataDash.projectDomain.slice(4);
      }


      const appID = 'app_'+Math.random().toString(36).substr(2, 9);

      dataDash.APP_CONFIG = "";
      dataDash.APP_CONFIG += "export const APP_CONFIG = {\n";
      dataDash.APP_CONFIG += "  PROJECT_VERSION: 0.5,\n";
      dataDash.APP_CONFIG += `  PROJECT_NAME: '${dataDash.projectName}',\n`;
      dataDash.APP_CONFIG += `  PROJECT_SLOGAN: '${dataDash.projectSlogan}',\n`;
      dataDash.APP_CONFIG += `  PROJECT_DOMAIN: '${dataDash.projectDomain}',\n`;
      dataDash.APP_CONFIG += `  PROJECT_PACKAGES: {\n`;
      dataDash.APP_CONFIG += `    DASHBOARD: {\n`;
      dataDash.APP_CONFIG += `      appManager: false,\n`;
      dataDash.APP_CONFIG += `      category: true,\n`;
      dataDash.APP_CONFIG += `      footer: true,\n`;
      dataDash.APP_CONFIG += `      logo: true,\n`;
      dataDash.APP_CONFIG += `      navHeader: true,\n`;
      dataDash.APP_CONFIG += `      page: true,\n`;
      dataDash.APP_CONFIG += `      product: true,\n`;
      dataDash.APP_CONFIG += `      settings: true\n`;
      dataDash.APP_CONFIG += `    },\n`;
      dataDash.APP_CONFIG += `    SHOP: {\n`;
      dataDash.APP_CONFIG += `      cart: true,\n`;
      dataDash.APP_CONFIG += `      cartBox: true,\n`;
      dataDash.APP_CONFIG += `      footer: true,\n`;
      dataDash.APP_CONFIG += `      loginBox: true,\n`;
      dataDash.APP_CONFIG += `      productGrid: true,\n`;
      dataDash.APP_CONFIG += `      searchBox: true,\n`;
      dataDash.APP_CONFIG += `      socialnetwork: true,\n`;
      dataDash.APP_CONFIG += `      topbarHeader: true,\n`;
      dataDash.APP_CONFIG += `      navFull: true\n`;
      dataDash.APP_CONFIG += `    },\n`;
      dataDash.APP_CONFIG += `    SITE: {\n`;
      dataDash.APP_CONFIG += `      footer: true,\n`;
      dataDash.APP_CONFIG += `      logo: true,\n`;
      dataDash.APP_CONFIG += `      navHeader: true,\n`;
      dataDash.APP_CONFIG += `      navFull: true\n`;
      dataDash.APP_CONFIG += `    },\n`;
      dataDash.APP_CONFIG += `  },\n`;
      dataDash.APP_CONFIG += `  MAIL_AUTH_VISITOR: 'visitante@${dataDash.projectDomain}',\n`;
      dataDash.APP_CONFIG += "  PATH_BASE: location.hostname\n";
      dataDash.APP_CONFIG += "}";

      dataDash.indexHTML = "";
      dataDash.indexHTML += "<!doctype html>";
      dataDash.indexHTML += '<html lang="en">';
        dataDash.indexHTML += "<head>";
          dataDash.indexHTML += '<meta charset="utf-8">';
          dataDash.indexHTML += '<meta name="viewport" content="width=device-width, initial-scale=1">';
          dataDash.indexHTML += '<link rel="shortcut icon" href="/favicon.ico">';
          dataDash.indexHTML += `<title>${dataDash.projectName} | ${dataDash.projectSlogan}</title>`;
          dataDash.indexHTML += '<link rel="stylesheet" href="/assets/css/app.css">';
          dataDash.indexHTML += "<link rel='stylesheet' id='options_typography_Ubuntu-css'  href='//fonts.googleapis.com/css?family=Ubuntu&#038;subset=latin' type='text/css' media='all' />";
          dataDash.indexHTML += "<link rel='stylesheet' id='options_typography_Josefin+Sans-css'  href='//fonts.googleapis.com/css?family=Josefin+Sans&#038;subset=latin' type='text/css' media='all' />";
          dataDash.indexHTML += "<link rel='stylesheet' id='options_typography_Pacifico-css'  href='//fonts.googleapis.com/css?family=Pacifico&#038;subset=latin' type='text/css' media='all' />";
        dataDash.indexHTML += "</head>";
        dataDash.indexHTML += "<body>";
          dataDash.indexHTML += '<div id="fb-root"></div>';
          dataDash.indexHTML += '<script>';
          dataDash.indexHTML += 'window.fbAsyncInit = function() {';
          dataDash.indexHTML += '  FB.init({';
          dataDash.indexHTML += "   appId      : '1302099989888765',";
          dataDash.indexHTML += "   xfbml      : true,";
          dataDash.indexHTML += "   version    : 'v2.9'";
          dataDash.indexHTML += '  });';
          dataDash.indexHTML += '  FB.AppEvents.logPageView();';
          dataDash.indexHTML += '};';
          dataDash.indexHTML += '(function(d, s, id) {';
            dataDash.indexHTML += 'var js, fjs = d.getElementsByTagName(s)[0];';
            dataDash.indexHTML += 'if (d.getElementById(id)) return;';
            dataDash.indexHTML += 'js = d.createElement(s); js.id = id;';
            dataDash.indexHTML += 'js.src = "//connect.facebook.net/pt_BR/sdk.js#xfbml=1&version=v2.8&appId=173126446404220";';
            dataDash.indexHTML += 'fjs.parentNode.insertBefore(js, fjs);';
          dataDash.indexHTML += "}(document, 'script', 'facebook-jssdk'));</script>";
          dataDash.indexHTML += `<div data-js="root" data-appID="${appID}"></div>`;
        dataDash.indexHTML += "</body>";
      dataDash.indexHTML += "</html>";
      dataDash.indexHTML += "";

      // fs.readFile(, (err, data) => {
      //   if(err)
      //     console.log('error: ',err);
      //   console.log(data);
        // console.log(dataDash.APP_CONFIG);
      // });

      rl.close();
      fs.writeFile(path.resolve('src/app/boot.config.js'), dataDash.APP_CONFIG, (err) => {
        if (err) throw err;
        console.log('             #####################################################');
        console.log(`             #### DashProject:   ${dataDash.projectName} - ${dataDash.projectSlogan}`);
        console.log('             #####################################################');
        console.log('             ########   Projeto configurado com sucesso.  ########');
        console.log('             #####################################################');
        fs.writeFile(path.resolve('public/index.html'), dataDash.indexHTML, (err) => {
          if (err) throw err;
        });
      });
    });
  });
});
