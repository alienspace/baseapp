const storage = window.localStorage;
export const nextStorage = {
  get: (item) => {
    return storage.getItem(item);
  },
  getJSON: (item) => {
    return JSON.parse(storage.getItem(item));
  },
  set: (key,value) => {
    if(typeof(value) === 'object')
      value = JSON.stringify(value);
    return storage.setItem(key,value);
  },
  update: (key,value) => {
    if(typeof(value) === 'object')
      value = JSON.stringify(value);
    return storage.setItem(key,value);
  },
  delete: (item) => {
    return storage.removeItem(item);
  },

  removeItemCART: (itemID) => {
    let nextDash = nextStorage.getJSON('nextDash');
    let list = nextDash.CART_productsList;
    // console.log('qty antes',Session.get('CART_countItems'));
    // eslint-disable-next-line
    list.map((product,key) => {
      if(product.id === itemID){
        // eslint-disable-next-line
        let qty = (nextDash.CART_countItems) - parseInt(product.productQty); //retirando qty do total
        nextDash.CART_countItems = qty; //atualiza quantidade do carrinho
        list.pop(key); //remove item
        // console.log('Remove success',itemID);

        nextDash.CART_productsList = list; //atualiza lista do carrinho
        nextStorage.set('nextDash',nextDash); //atualiza nextStorage
      }
      // console.log('List updated',list);
    });
  }
}
