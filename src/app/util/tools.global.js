import React from 'react';
import moment from 'moment';
import firebase from 'firebase';
import { APP_CONFIG } from './../boot.config';
export const TOOLS = {
  uniqueID: () => {
    return '_' + Math.random().toString(36).substr(2, 9);
  },
  getDate: () => {
    moment.locale('en');
    // let
    //   d = moment().format('YYYY-DD-MM'),
    //   dateCurrent = moment().format('YYYY/DD/MM'),
    //   HoursCurrent = moment().format('hh:mm:ss');

    // return dateCurrent + ' - ' + HoursCurrent;
    return moment().format();

  },
  orderbyPostsDate: (posts, order) => {
    if(!order)
      order='asc'
    moment.locale('en');
    return posts.sort((a,b) => {
      let
        keyA = moment(a.updateAt).format(),
        keyB = moment(b.updateAt).format();
        // Compare the 2 dates
        if(order === 'asc' || order === 'ASC'){
          if(keyA < keyB) return -1;
          if(keyA > keyB) return 1;
        }

        if(order === 'desc' || order === 'DESC'){
          if(keyA < keyB) return 1;
          if(keyA > keyB) return -1;
        }

        return 0;
    });
  },
  convertInnerHTML: (html,iframe) => {
    if(iframe)
      return (
        <article>
          <iframe
            src={TOOLS.stripAllTags(html)}
            width="90%"
            height="370px"
            style={{border:0}}></iframe>
        </article>
      );

    if(!iframe)
      iframe={
        iframe:{
          active:false,
          src:null,
          width:'100%',
          height:'250px',
          frameborder:0,
          style:'border:0;',
          allowfullscreen:true
        }
      };

    return (
      <article dangerouslySetInnerHTML={ {__html: html} } />
    );
  },
  hasDecimalPart: (str) => {
    str = TOOLS.filterNumbersDotsAndCommas(str)

    return (
      str[str.length - 3] === '.' ||
      str[str.length - 3] === ','
    )
  },
  getDecimalSymbol: (str) => {
    str = TOOLS.filterNumbersDotsAndCommas(str);
    return str[str.length - 3];
  },
  filterNumbers: (str) => {
    let filteredStr = str.replace(/[^\d]/g, '');
    return filteredStr;
  },
  filterNumbersDotsAndCommas: (str) => {
    return str.replace(/[^\d.,]/g, '')
  },
  getMoney: (str) => {
    //getMoney traz str que pode ser passado para formatReal que insere virgula
    //*getMoney e formatReal geralmente trabalham juntos
    // eslint-disable-next-line
    return parseInt( str.replace(/[\D]+/g,'') )
  },
  formatReal: (int) => {
    //formatReal recebe int que receberá virgula
    let tmp = int+'';
    let neg = false;
    if(tmp.indexOf("-") === 0)
    {
        neg = true;
        tmp = tmp.replace("-","=");
    }

    if(tmp.length === 1) tmp = "0"+tmp

    tmp = tmp.replace(/([0-9]{2})$/g, ",$1");
    if( tmp.length > 6)
        tmp = tmp.replace(/([0-9]{3}),([0-9]{2}$)/g, ".$1,$2");

    if( tmp.length > 9)
        tmp = tmp.replace(/([0-9]{3}).([0-9]{3}),([0-9]{2}$)/g,".$1.$2,$3");

    if( tmp.length > 12)
        tmp = tmp.replace(/([0-9]{3}).([0-9]{3}).([0-9]{3}),([0-9]{2}$)/g,".$1.$2.$3,$4");

    if(tmp.indexOf(".") === 0) tmp = tmp.replace(".","");
    if(tmp.indexOf(",") === 0) tmp = tmp.replace(",","0,");

    return (neg ? '-'+tmp : tmp);
  },
  stripAllTags: (text) =>{
    return text.replace(/(<([^>]+)>)/ig,"");
  },
  requiredInput: (e) => {
    if(!e.target.value || e.target.value === ''){
      e.target.classList.add('required');
    }else{
      e.target.classList.remove('required');
    }
    return false;
  },
  orderByCustom: (array) => {
    let currentIndex = array.length, temporaryValue, randomIndex;
    while (0 !== currentIndex) {
      randomIndex = Math.floor(Math.random() * currentIndex);
      currentIndex -= 1;

      //
      temporaryValue = array[currentIndex];
      array[currentIndex] = array[randomIndex];
      array[randomIndex] = temporaryValue;
    }

    return array;
  },
  removeArrayItem: (arrayObject, start, end) => {
    arrayObject.splice(start, end);
    return arrayObject;
  },
  insertArrayItem: (arrayObject, pos, item) => {
    arrayObject.splice(pos, 0, item);
    return arrayObject;
  },

  onClickTargetBlank: (e) => {
    if(e){
      let href = e.target.attributes.href.value;
      e.preventDefault();
      console.log(href);
      return window.open(href, '_blank');
    }
  },

  loginVisitor: () => {
    if(navigator.onLine){
      firebase.auth().signInWithEmailAndPassword(APP_CONFIG.MAIL_AUTH_VISITOR, 'visitante')
        .then(function(user) {
          if(user.uid)
            console.log(user.email);
        });
    }
  }
}
