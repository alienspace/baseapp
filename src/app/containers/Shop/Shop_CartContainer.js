import React, { Component } from 'react';
import { APP_CONFIG } from './../../boot.config';
import { nextStorage } from './../../util/nextStorage';

import { PanelContent } from './../../components/Global/PanelContent/PanelContent';
export class Shop_CartContainer extends Component{
  constructor(props){
    super(props);
    this.state = {
      isLoading: true,
      currentUser: null,
      id: null
    }
    document.title = APP_CONFIG.PROJECT_NAME;

    this.renderView = this.renderView.bind(this);
  }

  componentDidMount(){
    document.title = APP_CONFIG.PROJECT_NAME + ' | ' + APP_CONFIG.PROJECT_SLOGAN;
  }

  renderView(){
    let nextDash = nextStorage.getJSON('nextDash');
    return (
      <PanelContent
        panelType={'ShopCart'}
        where={'shop'}
        currentUser={null}
        action={null}
        pageID={null}
        propsExtra={nextDash.CART_productsList}
        buttonsAction={null}
        />
    );
  }//renderView();

  render(){
    return this.renderView()
  }
}
