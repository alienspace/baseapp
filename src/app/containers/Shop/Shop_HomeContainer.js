import React, { Component } from 'react';
import { APP_CONFIG } from './../../boot.config';
// import { nextStorage } from './../../util/nextStorage';
import {
  Col, Carousel, Button, Image,
} from 'react-bootstrap';

import { PanelContent } from './../../components/Global/PanelContent/PanelContent';
export class Shop_HomeContainer extends Component{
  constructor(props){
    super(props);
    this.state = {
      isLoading: true,
      currentUser: null,
      id: null,
      products: [
        {
          productID: 0,
          title: 'Produto Model',
          category: 'Default',
          shortDescription: 'Bla bla bla productShortDescription',
          productQty: 2,
          price: '10,11'
        },
        {
          productID: 1,
          title: 'Produto Model',
          category: 'Default',
          shortDescription: 'Bla bla bla productShortDescription',
          productQty: 2,
          price: '10,11'
        },
        {
          productID: 2,
          title: 'Produto Model',
          category: 'Default',
          shortDescription: 'Bla bla bla productShortDescription',
          productQty: 2,
          price: '10,11'
        },
        {
          productID: 3,
          title: 'Produto Model',
          category: 'Default',
          shortDescription: 'Bla bla bla productShortDescription',
          productQty: 2,
          price: '10,11'
        },
        {
          productID: 4,
          title: 'Produto Model',
          category: 'Default',
          shortDescription: 'Bla bla bla productShortDescription',
          productQty: 2,
          price: '10,11'
        },
        {
          productID: 5,
          title: 'Produto Model',
          category: 'Default',
          shortDescription: 'Bla bla bla productShortDescription',
          productQty: 2,
          price: '10,11'
        },
        {
          productID: 6,
          title: 'Produto Model',
          category: 'Default',
          shortDescription: 'Bla bla bla productShortDescription',
          productQty: 2,
          price: '10,11'
        }
      ]
      // productsList: [{
      //   productID: 0,
      //   productTitle: 'Produto Model',
      //   productCategory: 'Default',
      //   productShortDescription: 'Bla bla bla productShortDescription',
      //   productQty: 2,
      //   productPrice: '10.11'
      // }]
    }
    document.title = APP_CONFIG.PROJECT_NAME;

    this.renderView = this.renderView.bind(this);
  }

  componentDidMount(){
    document.title = APP_CONFIG.PROJECT_NAME + ' | ' + APP_CONFIG.PROJECT_SLOGAN;
  }

  renderView(){
    return (
      <Col xs={12} md={12}>
        <Col xs={12} md={12} className={'no-padding boxProducts5x3'}>
          <Col md={4} xs={12} className={'no-paddingLeft'}>
            <Col md={12} xs={12} className={'no-padding product1'}>
              {/*<Image src={'assets/images/box_img.png'} responsive />*/}
            </Col>
            <Col md={12} xs={12} className={'no-padding product2'}>
              {/*<Image src={'assets/images/box_img_hover.png'} responsive />*/}
            </Col>
          </Col>

          <Col md={4} xs={12} className={'no-padding'}>
            <Col md={12} xs={12} className={'product3'}>
              {/*<Image src={'assets/images/box_imgMiddle.png'} responsive />*/}
            </Col>
          </Col>

          <Col md={4} xs={12} className={'no-paddingRight'}>
            <Col md={12} xs={12} className={'no-padding product4'}>
              {/*<Image src={'assets/images/box_img2.png'} responsive />*/}
            </Col>
            <Col md={12} xs={12} className={'no-padding product5'}>
              {/*<Image src={'assets/images/box_img.png'} responsive />*/}
            </Col>
          </Col>
        </Col>

        <Col xs={12} md={8} className={'no-padding boxSpecialOffers'}>
          <h3>Promoções</h3>
          <Col xs={12} md={12} className={'no-padding'}>
          {!this.props.isMobile ?
            <Carousel
                indicators={false}
                controls={true}
                interval={3000}
                direction={'prev'}>
              <Carousel.Item animateIn={true} animateOut={true} direction={'prev'}>
                <Col xs={12} md={4} className={'productItem'}>
                  <Image src={'assets/images/product_special_offers.png'} responsive />
                  <h4>Impulse Sealer Heat Seal Machine</h4>
                  <div className={'priceContainer'}>
                    <span className={'price'}>R$ 23,50</span>
                  </div>
                </Col>
                <Col xs={12} md={4} className={'productItem'}>
                  <Image src={'assets/images/product_special_offers.png'} responsive />
                  <h4>Impulse Sealer Heat Seal Machine</h4>
                  <div className={'priceContainer'}>
                    <span className={'price'}>R$ 23,50</span>
                  </div>
                </Col>
                <Col xs={12} md={4} className={'productItem'}>
                  <Image src={'assets/images/product_special_offers.png'} responsive />
                  <h4>Impulse Sealer Heat Seal Machine</h4>
                  <div className={'priceContainer'}>
                    <span className={'price'}>R$ 23,50</span>
                  </div>
                </Col>
              </Carousel.Item>
              <Carousel.Item animateIn={true} animateOut={true} direction={'next'}>
                <Col xs={12} md={4} className={'productItem'}>
                  <Image src={'assets/images/product_special_offers.png'} responsive />
                  <h4>Impulse Sealer Heat Seal Machine</h4>
                  <div className={'priceContainer'}>
                    <span className={'price'}>R$ 23,50</span>
                  </div>
                </Col>
                <Col xs={12} md={4} className={'productItem'}>
                  <Image src={'assets/images/product_special_offers.png'} responsive />
                  <h4>Impulse Sealer Heat Seal Machine</h4>
                  <div className={'priceContainer'}>
                    <span className={'price'}>R$ 23,50</span>
                  </div>
                </Col>
                <Col xs={12} md={4} className={'productItem'}>
                  <Image src={'assets/images/product_special_offers.png'} responsive />
                  <h4>Impulse Sealer Heat Seal Machine</h4>
                  <div className={'priceContainer'}>
                    <span className={'price'}>R$ 23,50</span>
                  </div>
                </Col>
              </Carousel.Item>
            </Carousel>
          :
            <Carousel
                indicators={false}
                controls={true}
                interval={3000}
                direction={'prev'}>
              <Carousel.Item animateIn={true} animateOut={true} direction={'prev'}>
                <Col xs={12} md={4} className={'productItem'}>
                  <Image src={'assets/images/product_special_offers.png'} responsive />
                  <h4>Impulse Sealer Heat Seal Machine</h4>
                  <div className={'priceContainer'}>
                    <span className={'price'}>R$ 23,50</span>
                  </div>
                </Col>
              </Carousel.Item>
              <Carousel.Item animateIn={true} animateOut={true} direction={'next'}>
                <Col xs={12} md={4} className={'productItem'}>
                  <Image src={'assets/images/product_special_offers.png'} responsive />
                  <h4>Impulse Sealer Heat Seal Machine</h4>
                  <div className={'priceContainer'}>
                    <span className={'price'}>R$ 23,50</span>
                  </div>
                </Col>
              </Carousel.Item>
            </Carousel>
          }
          </Col>
        </Col>
        <Col xs={12} md={4} className={'boxProductFeatured pull-right'}>
          <h4>PLANNERS & DIARIES</h4>
          <span>RIFLE PAPER CO PARADISE CALENDAR</span>
          <figure>
            <Image src={'assets/images/boxProductFeatured.png'} responsive />
          </figure>
          <Button
            className={'btn btn-primary btn-extra no-radius'}
            block>
              <span>Comprar Agora!</span>
          </Button>
        </Col>
        <PanelContent
          panelType={'ShopProductGrid'}
          where={'shop'}
          currentUser={null}
          action={null}
          pageID={null}
          propsExtra={this.props.products}
          buttonsAction={null}
        />
      </Col>
    );
  }

  render(){
    return this.renderView()
  }
}
