import React, { Component } from 'react';
import {
  Button, Image,
  Grid, Col,
} from 'react-bootstrap'
import { APP_CONFIG } from './../../boot.config';
import { nextStorage } from './../../util/nextStorage';
import { TOOLS } from './../../util/tools.global';
import { browserHistory, Link } from 'react-router';
// import slug from 'slug';
import moment from 'moment';

/** CUSTOM COMPONENTS **/
// import { PanelContent } from './../../components/Global/PanelContent/PanelContent';
export class Shop_ProductDetailsContainer extends Component{
  constructor(props){
    super(props);
    this.state = {
      isLoading: true,
      currentUser: null,
      action: this.props.params.action || null,
      product_title: this.props.params.productslug,
      productslug: this.props.params.productslug || null,
      product_slug: browserHistory.getCurrentLocation().query.post || null,
      products: this.props.products || []
    }

    document.title = APP_CONFIG.PROJECT_NAME;

    this.facebookShare = this.facebookShare.bind(this);
    this.resetCurrentState = this.resetCurrentState.bind(this);
    this.listPostsCategory = this.listPostsCategory.bind(this);
    this.addItemCart = this.addItemCart.bind(this);
    this.renderProductDetails = this.renderProductDetails.bind(this);
  }

  componentWillMount(){
  }//componentWillMount();

  componentDidMount(){
    // console.log('browserHistory.getCurrentLocation().query.post ',browserHistory.getCurrentLocation().query.post)
    console.log('componentDidMount parms _> ',this.props.params);
    this.setState({
      isLoading: false
    });
  }//componentDidMount();

  componentWillReceiveProps(nextProps){
    console.log('componentWillReceiveProps ',nextProps);
    console.log('parms _> ',nextProps.params.product_slug);

    if(nextProps.params.product_slug !== this.state.product_slug){
      // console.log('(nextProps.location.params.categoryslug ',nextProps.location.params.categoryslug);
      return this.resetCurrentState(nextProps.params.product_slug);
    }
  }//componentWillReceiveProps();

  facebookShare(){
    console.log('window.location.href, ',window.location.href)
    window.FB.ui({
      method: 'share',
      mobile_iframe: true,
      href: window.location.href,
    }, function(response) {
          if (response && !response.error_message) {
            alert('Posting completed.');
          } else {
            alert('Error while posting.');
          }
        });
  }//facebookShare();

  resetCurrentState(product_slug){
    return this.setState({
      product_slug: product_slug
    });
  }//resetCurrentState();

  listPostsCategory(){
    if(!this.state.product_slug){
      if(this.props.products.length > 0 && !this.state.isLoading){
        // eslint-disable-next-line
        return this.props.products.map((product) => {
          // console.log('this.state.categoryslug ',this.state.categoryslug);
          // console.log('slug ',slug(page.category).toLowerCase());
          // if(slug(page.category).toLowerCase() === this.state.categoryslug)
          if(moment(product.updateAt).format() < moment().format()){
            let post_date = moment(product.updateAt).format('DD/MM/YYYY');
            return (
              <Col key={product.productID} className={'item'} xs={12} md={6}>
                <article className={'post_container'}>
                  <figure>
                    <Link
                      to={'/blog/'+product.slug}
                      title={product.title}>
                      {product.thumbnail ?
                        <Image src={product.thumbnail} responsive />
                      :
                        <Image src={'http://cpdmemorial.org/wp-content/themes/Artificial-Reason-WP/img/no_image.png'} responsive />
                      }
                    </Link>
                  </figure>
                  <Button className={'facebookShare fa fa-twitter'} onClick={this.facebookShare}></Button>
                  <Button className={'facebookShare fa fa-facebook'} onClick={this.facebookShare}></Button>
                  <h4 className={'post_title'}>{product.title}</h4>
                  <span className={'post_date'}>
                    <span>{post_date}</span>
                  </span>
                  <span className={'post_excerpt'}>{TOOLS.stripAllTags(product.content).slice(0,109)+'...'}</span>
                  <Link
                    to={'/blog/'+product.slug}
                    title={product.title}
                    className={'btn btn-primary btn-secondary btn-extra no-radius'}><span className={product.slug}>Ler matéria</span></Link>
                </article>
              </Col>
            );
          }
        });
      }else{
        return (
          <Col className={'spinner primary'}>
            <span className={'double-bounce'}></span>
            <span className={'double-bounce2'}></span>
          </Col>
        );
      }
    }
  }//listPostsCategory();

  addItemCart(e){
    e.preventDefault();
    // console.log('dataset click ',e.target.parentElement.parentElement.parentElement.children[0].dataset._id);
    let nextDash = nextStorage.getJSON('nextDash');

    let
      // productInner = e.target.parentElement.parentElement.parentElement.children[0],
      productID = e.target.dataset._id,
      productPhotoPrimary = e.target.parentElement.parentElement.parentElement.children[0].children[0].children[4].src,
      productTitle = e.target.dataset.title,
      productSlug = e.target.dataset.slug,
      productQty = e.target.dataset.qty,
      productPrice = e.target.dataset.price;

      /*
      console.log('productInner -> ', productInner);
      console.log('productID -> ', productID);
      console.log('productPhotoPrimary -> ', productPhotoPrimary);
      console.log('productTitle -> ', productTitle);
      console.log('productSlug -> ', productSlug);
      console.log('productQty -> ', productQty);
      console.log('productPrice -> ', productPrice);
      */

    let existItem=false;
    console.log('e.target.parentElement -> ',productPhotoPrimary);
    // eslint-disable-next-line
    nextDash.CART_productsList.map((product) => {
      if(product.productID === productID){
        // product.productPhotoPrimary = productPhotoPrimary;
        // eslint-disable-next-line
        product.productQty = parseInt(product.productQty) + parseInt(productQty);
        product.productSlug = product.slug;
        existItem = true;
      }
    });

    if(!existItem){
      nextDash.CART_productsList.push({
        productID:productID,
        productPhotoPrimary: productPhotoPrimary,
        productTitle:productTitle,
        productSlug: productSlug,
        productQty:productQty,
        productPrice:productPrice
      });
    }

    nextDash.CART_idOrder = nextDash.CART_idOrder;
    nextDash.CART_productsList = nextDash.CART_productsList;
    nextStorage.set('nextDash',nextDash);
    // console.log('nextStorage in ProductGrid -> ',nextStorage.getJSON('nextDash'));
    let currentPath = browserHistory.getCurrentLocation().pathname;
    browserHistory.push({
      pathname: currentPath,
      query: {"action":'updateCart'}
    });
  }//addItemCart(e);

  renderProductDetails(){
    // eslint-disable-next-line
    return this.props.products.map((product) => {
      if(product.slug === this.props.params.product_slug)
        return (
          <Col xs={12} md={12} key={product.productID} className={'item'}>
            <Col xs={12} md={6} className={'productPhoto'}>
              <figure>
                <span className={'lines top_left'}></span>
                <span className={'lines top_right'}></span>
                <span className={'lines bottom_left'}></span>
                <span className={'lines bottom_right'}></span>
                <Image src={product.photoPrimary} responsive alt={product.title} />
              </figure>
            </Col>
            <Col xs={12} md={6} className={'productDescription'}>
              <h1 className={'product_title'}>{product.title}</h1>
              <article>
                <div className={'shortDescription'}>
                  {TOOLS.convertInnerHTML(product.shortDescription)}
                </div>
                {TOOLS.convertInnerHTML(product.description)}
              </article>

              <Col xs={12} md={12} className={'productPriceContainer no-padding'}>
                <span>A partir de</span>
                <span className={'productPricePrimary'}>R$ {product.price}</span>
                <span className={'productUni'}>/ 30 un.</span>
              </Col>

              <Col xs={12} md={12} className={'productButtonContainer no-padding'}>
                <Button
                  ref={'comprar'}
                  onClick={this.addItemCart}
                  data-_id={product.productID}
                  data-title={product.title}
                  data-slug={product.slug}
                  data-qty={1}
                  data-price={product.price}
                  >
                  Configurar produto
                </Button>
                <span className={'spec'}>Na próxima tela você pode configurar as especificações do seu produto e conferir o preço</span>
              </Col>
            </Col>
          </Col>
        );
    });
  }//renderProductDetails();

  render(){
    if(this.props.params.product_slug){
      return (
        <Grid className={'productDetailsContainer singlePost'}>
          {this.renderProductDetails()}
        </Grid>
      );
    }
  }
}
