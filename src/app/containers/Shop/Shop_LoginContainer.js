import React, { Component } from 'react';
import { APP_CONFIG } from './../../boot.config';
// import { nextStorage } from './../../util/nextStorage';

import { PanelContent } from './../../components/Global/PanelContent/PanelContent';
export class Shop_LoginContainer extends Component{
  constructor(props){
    super(props);
    this.state = {
      isLoading: true,
      currentUser: null,
      id: null,
    }
    document.title = APP_CONFIG.PROJECT_NAME;

    this.renderView = this.renderView.bind(this);
  }

  componentDidMount(){
    document.title = APP_CONFIG.PROJECT_NAME + ' | ' + APP_CONFIG.PROJECT_SLOGAN;
  }

  renderView(){
    return (
      <PanelContent
        panelType={'ShopLogin'}
        where={'shop'}
        currentUser={this.props.currentUser}
        isVisitor={this.props.isVisitor}
        action={null}
        pageID={null}
        propsExtra={null}
        buttonsAction={null}
        />
    );
  }

  render(){
    return this.renderView()
  }
}
