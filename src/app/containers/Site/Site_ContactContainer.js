import React, { Component } from 'react';
import {
  Grid, Col, Button,
} from 'react-bootstrap';
import { TOOLS } from '../../util/tools.global';

/** CUSTOM COMPONENTS **/
export class Site_ContactContainer extends Component{
  constructor(props){
    super(props);

    this.state = {
      pages: this.props.pages || null,
    }

    this.renderPage = this.renderPage.bind(this);
  }

  renderPage(){
    // eslint-disable-next-line
    return this.props.pages.map((page) => {
      if(page.slug === 'contato'){
        return (
          <Col xs={12} md={12} key={page.pageID} className={'contact_form_container twoContainers'}>
            <h4 className={'page_title text-center'}>{page.title}</h4>
            <Col xs={12} md={6} className={'leftContainer'}>
              <form name={'contact_form'} className={'text-left'}>
                <label className={'input-group'}>
                  <span>Nome</span>
                  <input className={'input-control'} type={'text'} name={'name'} />
                </label>
                <label className={'input-group'}>
                  <span>E-mail</span>
                  <input className={'input-control'} type={'text'} name={'email'} />
                </label>
                <label className={'input-group'}>
                  <span>Telefone/Celular</span>
                  <input className={'input-control'} type={'text'} name={'phone_cel'} />
                </label>
                <label className={'input-group'}>
                  <span>Cidade/Uf</span>
                  <input className={'input-control'} type={'text'} name={'city_uf'} />
                </label>
                <label className={'input-group'}>
                  <span>Mensagem</span>
                  <textarea
                    className={'input-control'}
                    name={'message'}></textarea>
                </label>

                <label className={'input-group'}>
                  <Button
                    className={'input-control no-radius'}
                    name={'send'}>Enviar <i className={'fa fa-paper-plane'}></i></Button>
                </label>
              </form>
            </Col>
            <Col xs={12} md={6} className={'rightContainer'}>
              <Col xs={12} md={12} className={'no-padding text-left'}>
                <span className={'lines top_left'}></span>
                <span className={'lines top_right'}></span>
                <span className={'lines bottom_left'}></span>
                <span className={'lines bottom_right'}></span>
                {TOOLS.convertInnerHTML(page.content,{
                  iframe:{
                    active:true,
                    src:page.content,
                    width:'100%',
                    height:'420px',
                    frameborder:'0',
                    style:'border:0;',
                    allowfullscreen:true
                  }
                })}
              </Col>
            </Col>
          </Col>
        );
      }
    });
  }//renderPage();

  render(){
    return (
      <Grid className={'no-padding'}>
        {this.renderPage()}
      </Grid>
    );
  }
}
