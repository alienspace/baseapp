import React, { Component } from 'react';
import {
  Button,
  Col,
} from 'react-bootstrap'
import { APP_CONFIG } from './../../boot.config';
import { TOOLS } from './../../util/tools.global';
import { Link } from 'react-router';
import slug from 'slug';
import moment from 'moment';

/** CUSTOM COMPONENTS **/
// import { PanelContent } from './../../components/Global/PanelContent/PanelContent';
export class Site_BlogPostSingleContainer extends Component{
  constructor(props){
    super(props);
    this.state = {
      isLoading: true,
      currentUser: null,
      action: this.props.params.action || null,
      category_title: this.props.params.categoryslug,
      categoryslug: this.props.params.categoryslug || null,
      post_slug: this.props.params.post_slug || null,
      post_title: this.props.params.post_slug || null,
      pages: this.props.pages || []
    }

    document.title = APP_CONFIG.PROJECT_NAME;

    this.facebookShare = this.facebookShare.bind(this);
    this.resetCurrentState = this.resetCurrentState.bind(this);
    this.listPostsCategory = this.listPostsCategory.bind(this);
  }

  componentWillMount(){
    console.log(this.props.params);
    if(this.state.post_slug){
      // eslint-disable-next-line
      return this.props.pages.map((page) => {
        if(page.slug === this.state.post_slug)
          return this.setState({
            post_title: page.title
          });
      });
    }
  }//componentWillMount();

  componentDidMount(){
  }//componentDidMount();

  componentWillReceiveProps(nextProps){
    console.log('componentWillReceiveProps ',nextProps);
    console.log('parms _> ',this.props.params);

    if(nextProps.params.categoryslug !== this.state.categoryslug){
      // console.log('(nextProps.location.params.categoryslug ',nextProps.location.params.categoryslug);
      return this.resetCurrentState(nextProps.params.categoryslug);
    }
  }//componentWillReceiveProps();

  facebookShare(){
    console.log('window.location.href, ',window.location.href)
    window.FB.ui({
      method: 'share',
      mobile_iframe: true,
      href: window.location.href,
    }, function(response) {
          if (response && !response.error_message) {
            alert('Posting completed.');
          } else {
            alert('Error while posting.');
          }
        });
  }//facebookShare();

  resetCurrentState(categoryslug){
    // eslint-disable-next-line
    return this.props.categories.map((category) => {
      if(category.slug === categoryslug)
        return this.setState({
          categoryslug: categoryslug,
          category_title: category.title
        });
    });
  }//resetCurrentState();

  listPostsCategory(){
    if(this.state.categoryslug){
      if(this.props.pages.length > 0){
        // eslint-disable-next-line
        return TOOLS.orderbyPostsDate(this.props.pages, 'desc').map((page) => {
          // console.log('this.state.categoryslug ',this.state.categoryslug);
          // console.log('slug ',slug(page.category).toLowerCase());
          if(slug(page.category).toLowerCase() === this.state.categoryslug){
            if(moment(page.updateAt).format() < moment().format()){
              let post_date = moment(page.updateAt).format('DD/MM/YYYY hh:mm:ss');
              return (
                <Col key={page.pageID} className={'item'} xs={12} md={6}>
                  <article className={'post_container'}>
                    <Button className={'facebookShare fa fa-twitter'} onClick={this.facebookShare}></Button>
                    <Button className={'facebookShare fa fa-facebook'} onClick={this.facebookShare}></Button>
                    <h4 className={'post_slug'}>{page.title}</h4>
                    <span className={'post_date'}>
                      <span>{post_date}</span>
                    </span>
                    <span className={'post_excerpt'}>{TOOLS.stripAllTags(page.content).slice(0,109)+'...'}</span>
                    <Link
                      to={this.state.categoryslug+'/'+page.slug}
                      className={'btn btn-primary btn-secondary btn-extra no-radius'}><span>Ler matéria</span></Link>
                  </article>
                </Col>
              );
            }
          }
        });
      }
    }else{
      if(this.props.pages.length > 0){
        // eslint-disable-next-line
        return TOOLS.orderbyPostsDate(this.props.pages, 'desc').map((page) => {
          // console.log('this.state.categoryslug ',this.state.categoryslug);
          // console.log('slug ',slug(page.category).toLowerCase());
          if(moment(page.updateAt).format() < moment().format()){
            let post_date = moment(page.updateAt).format('DD/MM/YYYY hh:mm:ss');
            return (
              <Col key={page.pageID} className={'item'} xs={12} md={6}>
                <article className={'post_container'}>
                  <h4 className={'post_slug'}>{page.title}</h4>
                  <span className={'post_date'}>
                    <span>{post_date}</span>
                  </span>
                  <span className={'post_excerpt'}>{TOOLS.stripAllTags(page.content).slice(0,109)+'...'}</span>
                  <Link
                    to={this.state.categoryslug+'/'+page.slug}
                    className={'btn btn-primary btn-secondary btn-extra no-radius'}><span>Ler matéria</span></Link>
                </article>
              </Col>
            );
          }
        });
      }
    }
  }//listPostsCategory();

  renderPost(){
    return this.props.pages.map((page) => {
      if(page.slug === this.state.post_slug)
        return (
          <Col xs={12} md={12} className={'no-padding'}>
            <article>
              {page.content}
            </article>
          </Col>
        );
    });
  }//renderPost();

  render(){
    return (<h1>SinglePost</h1>);
  }
}
