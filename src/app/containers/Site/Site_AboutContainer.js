import React, { Component } from 'react';
import {
  Grid, Col,
} from 'react-bootstrap';
// import { Link } from 'react-router';
import { TOOLS } from '../../util/tools.global';

/** CUSTOM COMPONENTS **/
export class Site_AboutContainer extends Component{
  constructor(props){
    super(props);

    this.state = {
      appID : document.querySelector('[data-js="root"]').dataset.appid || null,
      pages: []
    }

    this.renderPage = this.renderPage.bind(this);
  }

  componentWillMount(){
    // let page = firebase.database().ref('apps/'+this.state.appID+'/pages/');
  }

  renderPage(){
    // eslint-disable-next-line
    return this.props.pages.map((page) => {
      if(page.slug === 'a-empresa'){
        return (
          <Col xs={12} md={12} key={page.pageID}>
            <h4 className={'page_title'}>{page.title}</h4>
            {TOOLS.convertInnerHTML(page.content)}
          </Col>
        );
      }
    });
  }//renderPage();

  render(){
    return (
      <Grid className={'no-padding'}>
        {this.renderPage()}
      </Grid>
    );
  }
}
