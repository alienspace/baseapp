import React, { Component } from 'react';
import {
  Button, Image,
  Grid, Col,
} from 'react-bootstrap'
import { APP_CONFIG } from './../../boot.config';
import { TOOLS } from './../../util/tools.global';
import { browserHistory, Link } from 'react-router';
// import slug from 'slug';
import moment from 'moment';

/** CUSTOM COMPONENTS **/
// import { PanelContent } from './../../components/Global/PanelContent/PanelContent';
export class Site_BlogContainer extends Component{
  constructor(props){
    super(props);
    this.state = {
      isLoading: true,
      currentUser: null,
      action: this.props.params.action || null,
      category_title: this.props.params.categoryslug,
      categoryslug: this.props.params.categoryslug || null,
      post_slug: browserHistory.getCurrentLocation().query.post || null,
      pages: this.props.pages || []
    }

    document.title = APP_CONFIG.PROJECT_NAME;

    this.facebookShare = this.facebookShare.bind(this);
    this.resetCurrentState = this.resetCurrentState.bind(this);
    this.listPostsCategory = this.listPostsCategory.bind(this);
    this.renderPost = this.renderPost.bind(this);
  }

  componentWillMount(){
  }//componentWillMount();

  componentDidMount(){
    // console.log('browserHistory.getCurrentLocation().query.post ',browserHistory.getCurrentLocation().query.post)
    // console.log('componentDidMount parms _> ',this.props.params);
    this.setState({
      isLoading: false
    });
  }//componentDidMount();

  componentWillReceiveProps(nextProps){
    // console.log('componentWillReceiveProps ',nextProps);
    // console.log('parms _> ',nextProps.params.post_slug);

    if(nextProps.params.post_slug !== this.state.post_slug){
      // console.log('(nextProps.location.params.categoryslug ',nextProps.location.params.categoryslug);
      return this.resetCurrentState(nextProps.params.post_slug);
    }
  }//componentWillReceiveProps();

  facebookShare(){
    console.log('window.location.href, ',window.location.href)
    window.FB.ui({
      method: 'share',
      mobile_iframe: true,
      href: window.location.href,
    }, function(response) {
          if (response && !response.error_message) {
            alert('Posting completed.');
          } else {
            alert('Error while posting.');
          }
        });
  }//facebookShare();

  resetCurrentState(post_slug){
    return this.setState({
      post_slug: post_slug
    });
  }//resetCurrentState();

  listPostsCategory(){
    if(!this.state.post_slug){
      if(this.props.pages.length > 0 && !this.state.isLoading){
        // eslint-disable-next-line
        return TOOLS.orderbyPostsDate(this.props.pages, 'desc').map((page) => {
          // console.log('this.state.categoryslug ',this.state.categoryslug);
          // console.log('slug ',slug(page.category).toLowerCase());
          // if(slug(page.category).toLowerCase() === this.state.categoryslug)
          if(moment(page.updateAt).format() < moment().format() && page.category !== false){
            let post_date = moment(page.updateAt).format('DD/MM/YYYY');
            return (
              <Col key={page.pageID} className={'item'} xs={12} md={6}>
                <article className={'post_container'}>
                  <figure>
                    <Link
                      to={'/blog/'+page.slug}
                      title={page.title}>
                      {page.thumbnail ?
                        <Image src={page.thumbnail} responsive />
                      :
                        <Image src={'http://cpdmemorial.org/wp-content/themes/Artificial-Reason-WP/img/no_image.png'} responsive />
                      }
                    </Link>
                  </figure>
                  <Button className={'facebookShare fa fa-twitter'} onClick={this.facebookShare}></Button>
                  <Button className={'facebookShare fa fa-facebook'} onClick={this.facebookShare}></Button>
                  <h4 className={'post_title'}>{page.title}</h4>
                  <span className={'post_date'}>
                    <span>{post_date}</span>
                  </span>
                  <span className={'post_excerpt'}>{TOOLS.stripAllTags(page.content).slice(0,109)+'...'}</span>
                  <Link
                    to={'/blog/'+page.slug}
                    title={page.title}
                    className={'btn btn-primary btn-secondary btn-extra no-radius'}><span className={page.slug}>Ler matéria</span></Link>
                </article>
              </Col>
            );
          }
        });
      }else{
        return (
          <Col className={'spinner primary'}>
            <span className={'double-bounce'}></span>
            <span className={'double-bounce2'}></span>
          </Col>
        );
      }
    }
  }//listPostsCategory();

  renderPost(){
    // eslint-disable-next-line
    return this.props.pages.map((page) => {
      if(page.slug === this.state.post_slug)
        return (
          <Col xs={12} md={12} key={page.pageID}>
            <Button className={'facebookShare fa fa-twitter'} onClick={this.facebookShare}></Button>
            <Button className={'facebookShare fa fa-facebook'} onClick={this.facebookShare}></Button>
            <h3 className={'post_title'}>{page.title}</h3>
            <span className={'post_date'}>{moment(page.createAt).format('DD/MM/YYYY - hh:mm:ss')}</span>
            <figure>
              <Image src={page.thumbnail} responsive alt={page.title} />
            </figure>
            <article className={'postContainer'}>
              <span className={'lines top_left'}></span>
              <span className={'lines top_right'}></span>
              <span className={'lines bottom_left'}></span>
              <span className={'lines bottom_right'}></span>
              {TOOLS.convertInnerHTML(page.content)}
            </article>
          </Col>
        );
    });
  }//renderPost();

  render(){
    if(this.state.post_slug){
      return (
        <Grid className={'blogContainer singlePost'}>
          <meta property="og:type"          content="website" />
          <meta property="og:title"         content="Gráfica Volare | Post blog" />
          <meta property="og:description"   content="Gráfica Rápida Digital" />
          <meta property="og:image"         content="https://graficavolare.devsuccess.com.br/assets/images/logo_volare.png" />
          {this.renderPost()}
        </Grid>
      );
    }else{
      return (
        <Grid className={'no-padding blogContainer'}>
          <h3 className={'page_title'}>Blog</h3>
          {this.listPostsCategory()}
        </Grid>
      );
    }
  }
}
