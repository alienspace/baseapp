import React, { Component } from 'react';
import {
  Image, Button, Carousel,
  Grid, Col,
} from 'react-bootstrap';
// import { Link } from 'react-router';

/** CUSTOM COMPONENTS **/
export class Site_HomeContainer extends Component{
  render(){
    return (
      <Grid className={'no-padding'}>
        <Col xs={12} md={12} className={'no-padding boxProducts5x3'}>
          <Col md={4} xs={12} className={'no-paddingLeft'}>
            <Col md={12} xs={12} className={'no-padding product1'}>
              {/*<Image src={'assets/images/box_img.png'} responsive />*/}
            </Col>
            <Col md={12} xs={12} className={'no-padding product2'}>
              {/*<Image src={'assets/images/box_img_hover.png'} responsive />*/}
            </Col>
          </Col>

          <Col md={4} xs={12} className={'no-padding'}>
            <Col md={12} xs={12} className={'product3'}>
              {/*<Image src={'assets/images/box_imgMiddle.png'} responsive />*/}
            </Col>
          </Col>

          <Col md={4} xs={12} className={'no-paddingRight'}>
            <Col md={12} xs={12} className={'no-padding product4'}>
              {/*<Image src={'assets/images/box_img2.png'} responsive />*/}
            </Col>
            <Col md={12} xs={12} className={'no-padding product5'}>
              {/*<Image src={'assets/images/box_img.png'} responsive />*/}
            </Col>
          </Col>
        </Col>

        <Col xs={12} md={8} className={'no-padding boxSpecialOffers'}>
          <h3>Promoções</h3>
          <Col xs={12} md={12} className={'no-padding'}>
                {!this.props.isMobile ?
                  <Carousel
                      indicators={false}
                      controls={true}
                      interval={3000}
                      direction={'prev'}>
                    <Carousel.Item animateIn={true} animateOut={true} direction={'prev'}>
                      <Col xs={12} md={4} className={'productItem'}>
                        <Image src={'assets/images/product_special_offers.png'} responsive />
                        <h4>Impulse Sealer Heat Seal Machine</h4>
                        <div className={'priceContainer'}>
                          <span className={'price'}>R$ 23,50</span>
                        </div>
                      </Col>
                      <Col xs={12} md={4} className={'productItem'}>
                        <Image src={'assets/images/product_special_offers.png'} responsive />
                        <h4>Impulse Sealer Heat Seal Machine</h4>
                        <div className={'priceContainer'}>
                          <span className={'price'}>R$ 23,50</span>
                        </div>
                      </Col>
                      <Col xs={12} md={4} className={'productItem'}>
                        <Image src={'assets/images/product_special_offers.png'} responsive />
                        <h4>Impulse Sealer Heat Seal Machine</h4>
                        <div className={'priceContainer'}>
                          <span className={'price'}>R$ 23,50</span>
                        </div>
                      </Col>
                    </Carousel.Item>
                    <Carousel.Item animateIn={true} animateOut={true} direction={'next'}>
                      <Col xs={12} md={4} className={'productItem'}>
                        <Image src={'assets/images/product_special_offers.png'} responsive />
                        <h4>Impulse Sealer Heat Seal Machine</h4>
                        <div className={'priceContainer'}>
                          <span className={'price'}>R$ 23,50</span>
                        </div>
                      </Col>
                      <Col xs={12} md={4} className={'productItem'}>
                        <Image src={'assets/images/product_special_offers.png'} responsive />
                        <h4>Impulse Sealer Heat Seal Machine</h4>
                        <div className={'priceContainer'}>
                          <span className={'price'}>R$ 23,50</span>
                        </div>
                      </Col>
                      <Col xs={12} md={4} className={'productItem'}>
                        <Image src={'assets/images/product_special_offers.png'} responsive />
                        <h4>Impulse Sealer Heat Seal Machine</h4>
                        <div className={'priceContainer'}>
                          <span className={'price'}>R$ 23,50</span>
                        </div>
                      </Col>
                    </Carousel.Item>
                  </Carousel>
                :
                  <Carousel
                      indicators={false}
                      controls={true}
                      interval={3000}
                      direction={'prev'}>
                    <Carousel.Item animateIn={true} animateOut={true} direction={'prev'}>
                      <Col xs={12} md={4} className={'productItem'}>
                        <Image src={'assets/images/product_special_offers.png'} responsive />
                        <h4>Impulse Sealer Heat Seal Machine</h4>
                        <div className={'priceContainer'}>
                          <span className={'price'}>R$ 23,50</span>
                        </div>
                      </Col>
                    </Carousel.Item>
                    <Carousel.Item animateIn={true} animateOut={true} direction={'next'}>
                      <Col xs={12} md={4} className={'productItem'}>
                        <Image src={'assets/images/product_special_offers.png'} responsive />
                        <h4>Impulse Sealer Heat Seal Machine</h4>
                        <div className={'priceContainer'}>
                          <span className={'price'}>R$ 23,50</span>
                        </div>
                      </Col>
                    </Carousel.Item>
                  </Carousel>
                }
          </Col>
        </Col>
        <Col xs={12} md={4} className={'boxProductFeatured pull-right'}>
          <h4>PLANNERS & DIARIES</h4>
          <span>RIFLE PAPER CO PARADISE CALENDAR</span>
          <figure>
            <Image src={'assets/images/boxProductFeatured.png'} responsive />
          </figure>
          <Button
            className={'btn btn-primary btn-extra no-radius'}
            block>
              <span>Comprar Agora!</span>
          </Button>
        </Col>
      </Grid>
    );
  }
}
