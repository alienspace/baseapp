import React, { Component } from 'react';
import {
  Col,
} from 'react-bootstrap';

/** CUSTOM COMPONENTS **/
export class NotFoundContainer extends Component{
  render(){
    return (
      <Col xs={12} md={12} className={'text-center'} style={{padding:2}}>
        <h1>Página não encontrada!</h1>
      </Col>
    );
  }
}
