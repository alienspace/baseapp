import React, { Component } from 'react';
import { APP_CONFIG } from './../../boot.config';
import { Auth } from './../../auth.core';
// import { nextStorage } from './../../util/nextStorage';
export class Dashboard_AuthContainer extends Component{
  constructor(props){
    super(props);
    this.state = {
      isLoading: true,
      currentUser: null,
      id: null,
    }
    document.title = APP_CONFIG.PROJECT_NAME;

    this.renderView = this.renderView.bind(this);
  }

  componentDidMount(){
    let _self = this;
    // let data = [];
    document.title = APP_CONFIG.PROJECT_NAME;
  }

  renderView(){
    // eslint-disable-next-line
    if( !this.props.currentUser || this.props.currentUser && this.props.currentUser.email === APP_CONFIG.MAIL_AUTH_VISITOR ){
      return (
        <Auth modeForm={'vertical'} />
      );
    }else{
      return (
        <div className={'panel panel-info'}>
          <div className="panel-heading">
            <h3 className="panel-title text-left">Painel Administrativo</h3>
          </div>
          <div className="panel-body">
            Quatro botoes gigantes.
          </div>
        </div>
      );
    }
  }

  render(){
    return (
      <div>
        {this.renderView()}
      </div>
    );
  }
}
