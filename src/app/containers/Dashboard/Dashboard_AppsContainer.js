import React, { Component } from 'react';

import { APP_CONFIG } from './../../boot.config';
import { Link } from 'react-router';

/** CUSTOM COMPONENTS **/
import { PanelContent } from './../../components/Global/PanelContent/PanelContent';
export class Dashboard_AppsContainer extends Component{
  constructor(props){
    super(props);
    this.state = {
      isLoading: true,
      currentUser: null,
      action: this.props.params.action,
      appID: this.props.params.appID ? this.props.params.appID : null,
      app_title: null
    }

    this.buttonsAction = this.buttonsAction.bind(this);
    document.title = APP_CONFIG.PROJECT_NAME + ' | Apps';
  }

  buttonsAction(){
    if( this.props.params.action === 'new' ){
      return (
        <h3 className="panel-title text-left">
          Novo App
          <Link to={'/dashboard/apps'} className="fa fa-floppy-o pull-right"> <span>Salvar</span></Link>
          <Link to={'/dashboard/apps'} className="fa fa-arrow-left pull-right"> <span>Cancelar</span></Link>
        </h3>
      );
    }else{
      return (
        <h3 className="panel-title text-left">
          Apps
          <Link to={'/dashboard/apps/new'} className="fa fa-plus-circle pull-right"> <span>Novo</span></Link>
        </h3>
      );
    }
  };//buttonsAction

  render(){
    return (
      <PanelContent
        panelType={'AppManager'}
        where={'dashboard'}
        currentUser={this.props.currentUser}
        action={this.props.params.action}
        appID={this.props.params.appID}
        buttonsAction={this.buttonsAction()}
        />
    );
  }
}
