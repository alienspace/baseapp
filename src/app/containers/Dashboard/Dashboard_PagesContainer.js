import React, { Component } from 'react';

import { APP_CONFIG } from './../../boot.config';
import { Link } from 'react-router';

/** CUSTOM COMPONENTS **/
import { PanelContent } from './../../components/Global/PanelContent/PanelContent';
export class Dashboard_PagesContainer extends Component{
  constructor(props){
    super(props);
    this.state = {
      isLoading: true,
      currentUser: null,
      action: this.props.params.action,
      pageID: this.props.params.pageID ? this.props.params.pageID : null,
      page_title: null
    }

    this.buttonsAction = this.buttonsAction.bind(this);
    document.title = APP_CONFIG.PROJECT_NAME;
  }

  buttonsAction(){
    if( this.props.params.action === 'new' ){
      return (
        <h3 className="panel-title text-left">
          Adicionar Conteúdo
          <Link to={'/dashboard/pages'} className="fa fa-floppy-o pull-right"> <span>Salvar</span></Link>
          <Link to={'/dashboard/pages'} className="fa fa-arrow-left pull-right"> <span>Cancelar</span></Link>
        </h3>
      );
    }else{
      return (
        <h3 className="panel-title text-left">
          Conteúdo
          <Link to={'/dashboard/pages/new'} className="fa fa-plus-circle pull-right"> <span>Adicionar</span></Link>
        </h3>
      );
    }
  };//buttonsAction

  render(){
    return (
      <PanelContent
        panelType={'page'}
        where={'dashboard'}
        currentUser={this.props.currentUser}
        action={this.props.params.action}
        pageID={this.props.params.pageID}
        buttonsAction={this.buttonsAction()}
        />
    );
  }
}
