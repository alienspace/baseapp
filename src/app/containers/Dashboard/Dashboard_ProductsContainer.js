import React, { Component } from 'react';

import { APP_CONFIG } from './../../boot.config';
import { Link } from 'react-router';

/** CUSTOM COMPONENTS **/
import { PanelContent } from './../../components/Global/PanelContent/PanelContent';
export class Dashboard_ProductsContainer extends Component{
  constructor(props){
    super(props);
    this.state = {
      currentUser: null,
      action: this.props.params.action,
      productID: this.props.params.productID ? this.props.params.productID : null,
      product_title: null
    }

    this.buttonsAction = this.buttonsAction.bind(this);
    document.title = APP_CONFIG.PROJECT_NAME;
  }

  buttonsAction(){
    if( this.props.params.action === 'new' ){
      return (
        <h3 className="panel-title text-left">
          Adicionar Produto
          <Link to={'/dashboard/products'} className="fa fa-floppy-o pull-right"> <span>Salvar</span></Link>
          <Link to={'/dashboard/products'} className="fa fa-arrow-left pull-right"> <span>Cancelar</span></Link>
        </h3>
      );
    }else{
      return (
        <h3 className="panel-title text-left">
          Todos os Produtos
          <Link to={'/dashboard/products/new'} className="fa fa-plus-circle pull-right"> <span>Adicionar</span></Link>
        </h3>
      );
    }
  };//buttonsAction

  render(){
    return (
      <PanelContent
        panelType={'product'}
        where={'dashboard'}
        currentUser={this.props.currentUser}
        action={this.props.params.action}
        productID={this.props.params.productID}
        buttonsAction={this.buttonsAction()}
        />
    );
  }
}
