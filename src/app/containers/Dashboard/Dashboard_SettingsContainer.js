import React, { Component } from 'react';

import { APP_CONFIG } from './../../boot.config';

/** CUSTOM COMPONENTS **/
import { PanelContent } from './../../components/Global/PanelContent/PanelContent';
export class Dashboard_SettingsContainer extends Component{
  constructor(props){
    super(props);
    this.state = {
      isLoading: true,
      currentUser: null,
      action: this.props.params.action,
      pageID: this.props.params.pageID ? this.props.params.pageID : null,
      page_title: null
    }

    document.title = APP_CONFIG.PROJECT_NAME;
    this.buttonsAction = this.buttonsAction.bind(this);
  }

  buttonsAction(){
    return (
      <h3 className="panel-title text-left">
        Configurações
      </h3>
    );
  };//buttonsAction();

  render(){
    return (
      <PanelContent
        panelType={'settings'}
        where={'dashboard'}
        currentUser={this.props.currentUser}
        action={this.props.params.action}
        pageID={null}
        buttonsAction={this.buttonsAction()}
        propsExtra={{
          logoURL:this.props.logoURL,
          packages:this.props.packages
        }}
        />
    );
  }
}
