import React, { Component } from 'react';
import {
  Col, Button,
} from 'react-bootstrap';
import { browserHistory } from 'react-router';
import firebase from 'firebase';

import { APP_CONFIG } from './../../boot.config';
/** CUSTOM COMPONENTS **/
export class SetupContainer extends Component{
  constructor(props){
    super(props);
    this.state = {
      uid : null,
      appID: document.querySelector('[data-js="root"]').dataset.appid || null
    }

    this.registerApp = this.registerApp.bind(this);
  }

  componentWillMount(){
    firebase.database().ref('apps/'+ this.state.appID).once('value').then((snapshot) => {
      if(snapshot.val() !== null)
        browserHistory.push('/dashboard');
    });
  }

  registerApp(){
    const dataInsert = {
      uid : this.state.uid,
      appID : this.state.appID,
      appData: {
        title : APP_CONFIG.PROJECT_NAME,
        slug : APP_CONFIG.PROJECT_NAME.toLowerCase(),
        description : 'installed in terminal',
        status: 'Publicado'
      },
      packages: APP_CONFIG.PROJECT_PACKAGES
    };
    firebase.database().ref('apps/'+dataInsert.appID)
      .set(dataInsert, () => {
        this.setState({
          isLoading : false,
          status_response : true
          // message_response : '{ ' + this.state.title + ' } gravado com sucesso!'
        });
        console.clear();
        window.location = "/";
      });
  }//registerApp();

  render(){
    return (
      <Col xs={12} md={12} className={'text-center'} style={{padding:2}}>
        <h1>Setup! - Step 1</h1>
        <Col xs={12} md={4} className={'col-md-offset-4 no-padding'}>
          <Button
            bsStyle={'success'}
            onClick={this.registerApp}
            block
          >Registrar App</Button>
        </Col>
      </Col>
    );
  }
}
