//https://p.w3layouts.com/demos/bike_shop/web/
//https://p.w3layouts.com/demos/luxury_watches/web/
/* eslint-disable */
import React, { Component } from 'react';
import {
  Grid, Row, Col, Carousel, Image
} from 'react-bootstrap';
import { Parallax } from 'react-parallax';
import { Link, browserHistory } from 'react-router';
import { nextStorage } from './util/nextStorage';
import { APP_CONFIG } from './boot.config';
import { TOOLS } from './util/tools.global';
import firebase from 'firebase';
import slug from 'slug';
// import { Auth } from './auth.core';

/** CUSTOM COMPONENTS **/
// import { Logo } from './components/Logo/Logo';
import { NavHeader } from './components/Global/NavHeader/NavHeader';
import { NavHeaderDashboard } from './components/Dashboard/NavHeader/NavHeader';
import { Footer } from './components/Site/Footer/Footer';
export class App extends Component {
  constructor(props){
    super(props);
    this.state = {
      appID: document.querySelector('[data-js="root"]').dataset.appid || null,

      currentUser: JSON.parse(window.sessionStorage.getItem('userSession')) || null,
      isMobile: false,
      portrait: false,
      config : {
        apiKey: "AIzaSyChqv1wMrwfj9DCnQYwwIyPseliw9_ETeM",
        authDomain: "volarestore-21140.firebaseapp.com",
        databaseURL: "https://volarestore-21140.firebaseio.com",
        projectId: "volarestore-21140",
        storageBucket: "volarestore-21140.appspot.com",
        messagingSenderId: "894456587474"
      },
      wifiConnected: navigator.onLine,
      firstVisitor: nextStorage.getJSON('firstVisitor') || false,
      isVisitor: nextStorage.getJSON('firstVisitor') || true,

      products: [],
      categories: [],
      pages: [],
      packages: {
        DASHBOARD: {
          appManager: false,
          category: false,
          footer: false,
          logo: false,
          navHeader: false,
          page: false,
          product: false,
          settings: false
        },
        SHOP: {
          cart: false,
          cartBox: false,
          footer: false,
          loginBox: false,
          productGrid: false,
          searchBox: false,
          socialnetwork: true,
          telephone: true,
          topbarHeader: false,
          navFull: false
        },
        SITE: {
          footer: true,
          logo: true,
          navHeader: true,
          navFull: true
        }
      }
    }

    this.connectFireBase = this.connectFireBase.bind(this);
    this.renderApp = this.renderApp.bind(this);

  };//constructor

  connectFireBase() {
    if(this.state.wifiConnected){
      firebase.initializeApp(this.state.config);
      console.log('initialized FireBaseConnect');
    }else{
      console.log('##########################');
      console.log('##### SEM CONEXÃO ;/ #####');
      console.log('##########################');
    }
  };//connectFireBase();

  componentWillMount(){
    let
      _self=this,
      nextDash = nextStorage.getJSON('nextDash') || {};

    if(this.state.wifiConnected){
      // console.log('Conexão estabelecida.');
      _self.connectFireBase();

      /** FIREBASE **/
      // console.log('User id -> ',this.state.uid);
      if(this.state.appID){
        firebase.database().ref('apps/'+ this.state.appID).once('value').then((snapshot) => {
          console.log('############### currentApp -> ',snapshot.val());
          if(snapshot.val() === null)
            browserHistory.push({pathname:'/dashboard/install',query:{step:1}});

          firebase.database().ref('apps/'+ this.state.appID + '/appData/logoURL').once('value').then((snapshot) => {
            _self.setState({logoURL: snapshot.val()});
          }).catch((err) => {
            console.log('error -> ',err);
          });
          console.log('CurrentApp -> ',this.state.appID);
        }).catch((err) => {
          console.log('error -> ',err);
        });
        // if(currentApp && currentApp[currentApp.length - 1] === this.state.appID){
        //
        //   // console.log('CurrentApp -> ',this.state.appID);
        // }else{

        // }
      }

      _self.allProducts = firebase.database().ref('apps/'+ this.state.appID +'/products/');
      _self.allCategories = firebase.database().ref('apps/'+ this.state.appID +'/categories/');
      _self.allPages = firebase.database().ref('apps/'+ this.state.appID +'/pages/');
      // _self.allProductsPhotos = firebase.storage().ref('apps/'+ this.state.appID +'/products/');
      // _self.allPackagesDashboard = firebase.database().ref('apps/'+ this.state.appID +'/packages/DASHBOARD/');
      _self.allPackagesShop = firebase.database().ref('apps/'+ this.state.appID +'/packages/SHOP/');
      _self.allPackagesSite = firebase.database().ref('apps/'+ this.state.appID +'/packages/SITE/');
      _self.products = this.state.products;
      _self.categories = this.state.categories;
      _self.pages = this.state.pages;
      _self.packages = this.state.packages;

      /** child_added **/
      _self.allProducts.on('child_added', (data) => {
        _self.products = this.state.products;
        _self.products.push(data.val());
        if(data){
          if(_self.products.length >= 0 ){
            _self.setState({ isLoading : false });
            _self.setState({ products : _self.products });
          }
        }
      });
      _self.allCategories.on('child_added', (data) => {
        // console.log('child_added');
        if(data){
          _self.categories.push(data.val());
          if(_self.categories.length > 0 )
            _self.setState({ isLoading : false });

          _self.setState({ categories : _self.categories });
        }
      });
      _self.allPages.on('child_added', (data) => {
        // console.log('child_added');
        _self.pages = this.state.pages;
        _self.pages.push(data.val());
        if(data){
          if(_self.pages.length >= 0 ){
            _self.setState({ isLoading : false });
            _self.setState({ pages : _self.pages });
          }
        }
      });

      /** PACKAGES **/
      // _self.allPackagesDashboard.on('child_added', (data) => {
      //   _self.packages = this.state.packages;
      //   switch (data.key) {
      //     case 'appManager':
      //       _self.packages.DASHBOARD.appManager = data.val();
      //       _self.setState({ packages : _self.packages });
      //       break;
      //     case 'category':
      //       _self.packages.DASHBOARD.category = data.val();
      //       _self.setState({ packages : _self.packages });
      //       break;
      //     case 'footer':
      //       _self.packages.DASHBOARD.footer = data.val();
      //       _self.setState({ packages : _self.packages });
      //       break;
      //     case 'logo':
      //       _self.packages.DASHBOARD.logo = data.val();
      //       _self.setState({ packages : _self.packages });
      //       break;
      //     case 'navHeader':
      //       _self.packages.DASHBOARD.navHeader = data.val();
      //       _self.setState({ packages : _self.packages });
      //       break;
      //     case 'page':
      //       _self.packages.DASHBOARD.page = data.val();
      //       _self.setState({ packages : _self.packages });
      //       break;
      //     case 'product':
      //       _self.packages.DASHBOARD.product = data.val();
      //       _self.setState({ packages : _self.packages });
      //       break;
      //     case 'settings':
      //       _self.packages.DASHBOARD.settings = data.val();
      //       _self.setState({ packages : _self.packages });
      //       break;
      //     default:
      //       console.log('Package não encontrado! ',data.key);
      //   }
      //   if(data){
      //     if(_self.packages.length >= 0 ){
      //       _self.setState({ isLoading : false });
      //     }
      //   }
      // });
      _self.allPackagesShop.on('child_added', (data) => {
        _self.packages = this.state.packages;
        // console.log('_self.packages -> ',_self.packages)
        // console.log('data.key -> ',data.key)
        switch (data.key) {
          case 'cart':
            _self.packages.SHOP.cart = data.val();
            _self.setState({ packages : _self.packages });
            break;
          case 'cartBox':
            _self.packages.SHOP.cartBox = data.val();
            _self.setState({ packages : _self.packages });
            break;
          case 'footer':
            _self.packages.SHOP.footer = data.val();
            _self.setState({ packages : _self.packages });
            break;
          case 'loginBox':
            _self.packages.SHOP.loginBox = data.val();
            _self.setState({ packages : _self.packages });
            break;
          case 'productGrid':
            _self.packages.SHOP.productGrid = data.val();
            _self.setState({ packages : _self.packages });
            break;
          case 'searchBox':
            _self.packages.SHOP.searchBox = data.val();
            _self.setState({ packages : _self.packages });
            break;
          case 'socialnetwork':
            _self.packages.SHOP.socialnetwork = data.val();
            _self.setState({ packages : _self.packages });
            break;
          case 'topbarHeader':
            _self.packages.SHOP.topbarHeader = data.val();
            _self.setState({ packages : _self.packages });
            break;
          case 'navFull':
            _self.packages.SHOP.navFull = data.val();
            _self.setState({ packages : _self.packages });
            break;
          default:
            console.log('Package não encontrado! ',data.key);
        }
        if(data){
          if(_self.packages.length >= 0 ){
            _self.setState({ isLoading : false });
          }
        }
      });
      _self.allPackagesSite.on('child_added', (data) => {
        _self.packages = this.state.packages;
        switch (data.key) {
          case 'footer':
            _self.packages.SITE.footer = data.val();
            _self.setState({ packages : _self.packages });
            break;
          case 'logo':
            _self.packages.SITE.logo = data.val();
            _self.setState({ packages : _self.packages });
            break;
          case 'navHeader':
            _self.packages.SITE.navHeader = data.val();
            _self.setState({ packages : _self.packages });
            break;
          case 'navFull':
            _self.packages.SITE.navFull = data.val();
            _self.setState({ packages : _self.packages });
            break;
          default:
            console.log('Package não encontrado! ',data.key);
        }
        if(data){
          if(_self.packages.length >= 0 ){
            _self.setState({ isLoading : false });
          }
        }
      });
      /** PACKAGES END **/
      /** child_added **/

      /** child_changed **/
      _self.allProducts.on('child_changed', (data) => {
        if(data){
          let changed = data.val();
          if(_self.products.length > 0 ){
            console.log('child_changed products: ', {changed : changed});
            // eslint-disable-next-line
            _self.products.map((product) => {
              if(product.productID === changed.productID){
                product.uid = this.state.uid;
                product.title = changed.title;
                product.slug = changed.slug;
                product.description = changed.description;
                product.shortDescription = changed.shortDescription;
                product.category = changed.category;
                product.typeContent = changed.typeContent;
                product.qtd = changed.qtd;
                product.price = changed.price;
                product.status = changed.status;
              }
            });
            _self.setState({ isLoading : false });
          }else{
            return false;
          }

          _self.setState({ products : _self.products });
        }
      });
      _self.allCategories.on('child_changed', (data) => {
        if(data){
          let changed = data.val();
          if(_self.allCategories.length > 0 ){
            console.log('child_changed allCategories: ', {changed : changed});
            // eslint-disable-next-line
            _self.allCategories.map((category) => {
              if(category.categoryID === changed.categoryID){
                category.uid = this.state.uid;
                category.title = changed.title;
                category.description = changed.description;
                category.status = changed.status;
              }
            });
            _self.setState({ isLoading : false });
          }else{
            return false;
          }

          _self.setState({ allCategories : _self.allCategories });
        }
      });
      _self.allPages.on('child_changed', (data) => {
        if(data){
          let changed = data.val();
          if(_self.pages.length > 0 ){
            console.log('child_changed pages: ', {changed : changed});
            // eslint-disable-next-line
            _self.pages.map((page) => {
              if(page.pageID === changed.pageID){
                page.uid = this.state.uid;
                page.title = changed.title;
                page.slug = changed.slug;
                page.description = changed.description;
                page.category = changed.category;
                page.typeContent = changed.typeContent;
                page.status = changed.status;
                page.createAt = changed.createAt;
                page.updateAt = changed.updateAt;
              }
            });
            _self.setState({ isLoading : false });
          }else{
            return false;
          }

          _self.setState({ pages : _self.pages });
        }
      });

      /** PACKAGES **/
      _self.allPackagesShop.on('child_changed', (data) => {
        if(data){
          let changed = {mod:data.key, status:data.val()};
          console.log('child_changed packages.SHOP: ', changed);

          switch (changed.mod) {
            case 'cart':
              _self.packages.SHOP.cart = changed.status;
              _self.setState({ packages : _self.packages });
              break;
            case 'cartBox':
              _self.packages.SHOP.cartBox = changed.status;
              _self.setState({ packages : _self.packages });
              break;
            case 'footer':
              _self.packages.SHOP.footer = changed.status;
              _self.setState({ packages : _self.packages });
              break;
            case 'loginBox':
              _self.packages.SHOP.loginBox = changed.status;
              _self.setState({ packages : _self.packages });
              break;
            case 'productGrid':
              _self.packages.SHOP.productGrid = changed.status;
              _self.setState({ packages : _self.packages });
              break;
            case 'searchBox':
              _self.packages.SHOP.searchBox = changed.status;
              _self.setState({ packages : _self.packages });
              break;
            case 'socialnetwork':
              _self.packages.SHOP.socialnetwork = changed.status;
              _self.setState({ packages : _self.packages });
              break;
            case 'topbarHeader':
              _self.packages.SHOP.topbarHeader = changed.status;
              _self.setState({ packages : _self.packages });
              break;
            case 'navFull':
              _self.packages.SHOP.navFull = changed.status;
              _self.setState({ packages : _self.packages });
              break;
            default:
              console.log('Package não encontrado! ',changed.mod);
          }

          _self.setState({ isLoading : false });
        }
      });
      /** PACKAGES END **/
      /** child_changed **/

      /** child_moved **/
      _self.allProducts.on('child_moved', (data) => {
        _self.products.push(data.val());
        if(data){
          if(_self.products.length > 0 )
            _self.setState({ isLoading : false });

          _self.setState({ products : _self.products });
        }
        console.log('child_moved products: ', {qtd: _self.products.length, all: _self.products});
      });
      _self.allCategories.on('child_moved', (data) => {
        _self.allCategories.push(data.val());
        if(data){
          if(_self.allCategories.length > 0 )
            _self.setState({ isLoading : false });

          _self.setState({ allCategories : _self.allCategories });
        }
        console.log('child_moved allCategories: ', {qtd: _self.allCategories.length, all: _self.allCategories});
      });
      _self.allPages.on('child_moved', (data) => {
        _self.pages.push(data.val());
        if(data){
          if(_self.pages.length > 0 )
            _self.setState({ isLoading : false });

          _self.setState({ pages : _self.pages });
        }
        console.log('child_moved pages: ', {qtd: _self.pages.length, all: _self.pages});
      });
      /** child_moved **/

      /** child_removed **/
      _self.allProducts.on('child_removed', (data) => {
        if(data){
          if(_self.products.length > 0 ){
            let productRemoved = data.val();
            console.log('res child_removed -> ',productRemoved);
            // eslint-disable-next-line
            _self.products.map((product, key) => {
              // eslint-disable-next-line
              if(product.productID === productRemoved.productID){
                console.log('Key -> ',key);
                TOOLS.removeArrayItem(_self.products,key,1);
                console.log('Product removeed ',productRemoved.productID);
                _self.setState({ products : _self.products, isLoading : false });
              }else{
                console.log('Key -> ',key);
                console.log('Product productID ',product.productID);
                console.log('Product removeed ',productRemoved.productID);
              }
            });
          }
        }
      });
      _self.allCategories.on('child_removed', (data) => {
        if(data){
          if(_self.categories.length > 0 ){
            let categoryRemoved = data.val();
            console.log('res child_removed -> ',categoryRemoved);
            // eslint-disable-next-line
            _self.categories.map((category, key) => {
              // eslint-disable-next-line
              if(category.categoryID === categoryRemoved.categoryID){
                console.log('Key -> ',key);
                TOOLS.removeArrayItem(key,1);
                console.log('Category removeed ',categoryRemoved.categoryID);
                _self.setState({ categories : _self.categories, isLoading : false });
              }else{
                console.log('Key -> ',key);
                console.log('Category categoryID ',category.categoryID);
                console.log('Category removeed ',categoryRemoved.categoryID);
              }
            });
          }
        }
      });
      _self.allPages.on('child_removed', (data) => {
        if(data){
          if(_self.pages.length > 0 ){
            let pageRemoved = data.val();
            console.log('res child_removed -> ',pageRemoved);
            // eslint-disable-next-line
            _self.pages.map((page, key) => {
              // eslint-disable-next-line
              if(page.pageID === pageRemoved.pageID){
                console.log('Key -> ',key);
                TOOLS.removeArrayItem(_self.pages,key,1);
                console.log('Page removeed ',pageRemoved.pageID);
                _self.setState({ pages : _self.pages, isLoading : false });
              }else{
                console.log('Key -> ',key);
                console.log('Page pageID ',page.pageID);
                console.log('Page removeed ',pageRemoved.pageID);
              }
            });
          }
        }
      });
      /** child_removed **/
      /** FIREBASE END **/

      if(!this.state.currentUser){
        this.setState({isVisitor:true});
        // nextStorage.set("firebase:authUser:AIzaSyBthxSpEn3BDtFB_H-KBJSefISf0mJYnlw:[DEFAULT]", '{"uid":"uxYJ5B7fw7UI5JMlM6bjLxpaLrb2","displayName":null,"photoURL":null,"email":"visitante@ultragrip.com.br","emailVerified":false,"isAnonymous":false,"providerData":[{"uid":"visitante@ultragrip.com.br","displayName":null,"photoURL":null,"email":"visitante@ultragrip.com.br","providerId":"password"}],"apiKey":"AIzaSyBthxSpEn3BDtFB_H-KBJSefISf0mJYnlw","appName":"[DEFAULT]","authDomain":"acreditareventos.firebaseapp.com","stsTokenManager":{"apiKey":"AIzaSyBthxSpEn3BDtFB_H-KBJSefISf0mJYnlw","refreshToken":"AJly3UXb9rQn12ajANHel4bTbt5jMGAJOos75qbWHXh6CCZIwokCskCwsaIsjgA141-AHylLM2R7gkTw01PxVI53SxkE3PNB5_OcnRjjN1bWuCJQ-nMJO-mTg4Gy_ieM8MizOwTLBx_V_vk9wU6SLjuYEcILECDfSy_nI6V_xXWvAEFVa2LF4AkrQoiMREgKmbTbvkw6UMarhx44xABC1MYiezVXP9zX9ZfHrSDrjdz486xT_jFEQuk","accessToken":"eyJhbGciOiJSUzI1NiIsImtpZCI6IjljY2RmMjc1NmVkYWUxYjk2YjdiMjhhYzU1MTNlZGYyOTlmMTU5MjgifQ.eyJpc3MiOiJodHRwczovL3NlY3VyZXRva2VuLmdvb2dsZS5jb20vYWNyZWRpdGFyZXZlbnRvcyIsImF1ZCI6ImFjcmVkaXRhcmV2ZW50b3MiLCJhdXRoX3RpbWUiOjE0OTE1OTQ4MzAsInVzZXJfaWQiOiJ1eFlKNUI3Znc3VUk1Sk1sTTZiakx4cGFMcmIyIiwic3ViIjoidXhZSjVCN2Z3N1VJNUpNbE02YmpMeHBhTHJiMiIsImlhdCI6MTQ5MTU5NDgzMCwiZXhwIjoxNDkxNTk4NDMwLCJlbWFpbCI6InZpc2l0YW50ZUB1bHRyYWdyaXAuY29tLmJyIiwiZW1haWxfdmVyaWZpZWQiOmZhbHNlLCJmaXJlYmFzZSI6eyJpZGVudGl0aWVzIjp7ImVtYWlsIjpbInZpc2l0YW50ZUB1bHRyYWdyaXAuY29tLmJyIl19LCJzaWduX2luX3Byb3ZpZGVyIjoicGFzc3dvcmQifX0.iUL33yNaItpN2GBQo3Yioqg8XmRrg6tjHyjixObQ-0weFVSSh1mJranuTz5BJFtMY7iZIBXHWNuCPe8UgzcjK0iwVqu5iWATjl0tjfC0puFW2Ba9NX0RF3yHc1XyA1_jxyPD71jEhvTHeA6uMShoPHJHSGtIBTTwQ5ZSduPl8eaTBnON_bLfwh4KiOaNEhJUlMp_lZ6rXT6W8qcu8mR5hZxOULrHBvdSZN88I177bstrc5iQH_qy36OxyXh-1PFILWgRsyhje6ZP6ykC2KxI_4PLpFL8MeX3yOAjkoXsatzdaxOJMsmeLQIujNoMAaqlS4ePe9vuQJge2Q0M3vGYcw","expirationTime":1491598430144},"redirectEventId":null}');
        // nextStorage.set("firebase:host:acreditareventos.firebaseio.com", '"s-usc1c-nss-127.firebaseio.com"');
        TOOLS.loginVisitor();
      }
      /** firstVisitor **/
      nextStorage.set('wifiConnected',this.state.wifiConnected);
      nextStorage.set('firstVisitor',true);
      nextStorage.set('isVisitor',true);

      this.setState({
        'firstVisitor': nextStorage.getJSON('isLogged'),
        'wifiConnected': nextStorage.getJSON('wifiConnected')
      });

      firebase.auth().onAuthStateChanged(function(user) {
        if(user)
          _self.setState({ currentUser : user });
      });
    }else{
      /*** ONLY ENV DEVELOPMENT ***/
      _self.setState({
        isVisitor:true,
        isLoading:false,
        appID : '--------notConnect--------',
        apps: [{
          appID : '--------notConnect--------',
          name : '--------notConnect--------',
          slug: slug('--------notConnect--------'),
          status: 'Publicado'
        }],
        currentUser : {
          uid: '--------notConnect--------',
          email: 'bruno@admin.com'
        },
        packages: {
          DASHBOARD: {
            appManager: false,
            category: false,
            footer: false,
            logo: false,
            navHeader: false,
            page: false,
            product: false,
            settings: false
          },
          SHOP: {
            cart: false,
            cartBox: false,
            footer: true,
            loginBox: true,
            productGrid: true,
            searchBox: false,
            socialnetwork: true,
            topbarHeader: true,
            navFull: true
          },
          SITE: {
            footer: true,
            logo: true,
            navHeader: true,
            navFull: true
          }
        }
      });
      _self.packages = _self.state.packages;
      console.log('_self.packages -> ',_self.packages);
      /*** ONLY ENV DEVELOPMENT ***/
      console.log('##########################');
      console.log('##### SEM CONEXÃO ;/ #####');
      console.log('##########################');

      /** firstVisitor **/
      if(nextStorage.getJSON('firstVisitor')){
        this.setState({
          'firstVisitor': nextStorage.getJSON('isLogged'),
          'wifiConnected': nextStorage.getJSON('wifiConnected'),
          'isVisitor': nextStorage.getJSON('isVisitor')
        });
      }else{

      }
      // nextStorage.set('isLogged',true);
      // nextStorage.set('isOnline',false);
    }

    const root = document.querySelector('[data-js="root"]'),
          currentPath = browserHistory.getCurrentLocation().pathname;
    if(currentPath.split('/')[1] === 'dashboard' || currentPath.split('/')[1] === 'admin'){
      root.classList.add('dashboard');
    }

    //if não existe um idOrder/Session... gera e seta. :)
    let dateSession = new Date();
    if(!nextDash.CART_idOrder){
      nextStorage.set('nextDash',{
        CART_appID: this.state.appID,
        CART_idOrder: TOOLS.uniqueID() + '_' + dateSession.getTime() + '_' + dateSession.getFullYear(),
        CART_countItems: 0,
        CART_productsList: []
      });
    }else{
      /*
       * if já exite um idOrder/Session...
       * apenas verificamos se há produtos no carrinho...
       */
      //  console.log('nextDash CART_productsList in App -> ',nextDash.CART_productsList);
      if(nextDash.CART_productsList && nextDash.CART_productsList.length > 0){
        //if há produtos no carrinho da sessão...
        console.log(`Há (${nextDash.CART_productsList.length}) itens no carrinho -> `,nextDash.CART_productsList);
      }else{
        console.log('Não há produtos no carrinho -> ',nextDash.CART_productsList);
      }
    }
    console.log('nextStorage in App -> ',nextStorage.getJSON('nextDash'));
  };//componentWillMount

  componentDidMount(){
    let _self=this;
    this.isMobile();

    window.addEventListener('scroll', () => {
      const root = document.querySelector('[data-js="root"]');
      let nav = document.getElementsByClassName('navHeader');
      // if(screen.width > 768){
        if(window.scrollY > 40){
          nav[0].classList.add('scrolling');
          root.classList.add('scrolling');
        }else{
          nav[0].classList.remove('scrolling');
          root.classList.remove('scrolling');
        }
      // }
    });

    _self.packages = _self.state.packages;
    // console.log('_self.packages2 -> ',_self.packages);
    // console.log('didMount');

    /** child_added **/
    // _self.allPages.on('child_added', (data) => {
    //   // console.log('child_added');
    //   _self.pages = this.state.pages;
    //   _self.pages.push(data.val());
    //   if(data){
    //     if(_self.pages.length >= 0 ){
    //       _self.setState({ isLoading : false });
    //       _self.setState({ pages : _self.pages });
    //     }
    //   }
    // });
    /** child_added **/

    /** child_changed **/
    // _self.allPages.on('child_changed', (data) => {
    //   if(data){
    //     let changed = data.val();
    //     if(_self.pages.length > 0 ){
    //       console.log('child_changed pages: ', {changed : changed});
    //       // eslint-disable-next-line
    //       _self.pages.map((page) => {
    //         if(page.pageID === changed.pageID){
    //           page.uid = this.state.uid;
    //           page.title = changed.title;
    //           page.slug = changed.slug;
    //           page.description = changed.description;
    //           page.category = changed.category;
    //           page.typeContent = changed.typeContent;
    //           page.status = changed.status;
    //           page.createAt = changed.createAt;
    //           page.updateAt = changed.updateAt;
    //         }
    //       });
    //       _self.setState({ isLoading : false });
    //     }else{
    //       return false;
    //     }
    //
    //     _self.setState({ pages : _self.pages });
    //   }
    // });
    /** child_changed **/

    /** child_moved **/
    // _self.allPages.on('child_moved', (data) => {
    //   _self.pages.push(data.val());
    //   if(data){
    //     if(_self.pages.length > 0 )
    //       _self.setState({ isLoading : false });
    //
    //     _self.setState({ pages : _self.pages });
    //   }
    //   console.log('child_moved pages: ', {qtd: _self.pages.length, all: _self.pages});
    // });
    /** child_moved **/

    /** child_removed **/
    // _self.allPages.on('child_removed', (data) => {
    //   if(data){
    //     if(_self.pages.length > 0 ){
    //       let pageRemoved = data.val();
    //       console.log('res child_removed -> ',pageRemoved);
    //       // eslint-disable-next-line
    //       _self.pages.map((page, key) => {
    //         // eslint-disable-next-line
    //         if(page.pageID === pageRemoved.pageID){
    //           console.log('Key -> ',key);
    //           TOOLS.removeArrayItem(_self.pages,key,1);
    //           console.log('Page removeed ',pageRemoved.pageID);
    //           _self.setState({ pages : _self.pages, isLoading : false });
    //         }else{
    //           console.log('Key -> ',key);
    //           console.log('Page pageID ',page.pageID);
    //           console.log('Page removeed ',pageRemoved.pageID);
    //         }
    //       });
    //     }
    //   }
    // });
    /** child_removed **/
    /** FIREBASE END **/
  };//componentDidMount

  checkOrientation(){
    let _self=this;
    window.addEventListener("orientationchange", function() {
      if (window.orientation === 90 || window.orientation === -90) {
        // console.log('landscape');
        // alert('Site melhor visualizado em portrait!');
        return _self.setState({ portrait: false }); //landscape
      } else {
        // console.log('portrait');
        return _self.setState({ portrait: true });
      }
    });
  };//checkOrientation();

  isMobile(){
    let viewport = window.innerWidth;
    if(viewport <= 768){
      this.setState({
        isMobile: true,
      });
      this.checkOrientation();
    }
  };//isMobile();

  renderApp(){
    const currentPath = browserHistory.getCurrentLocation().pathname;
    let pageName = currentPath.split('/')[1];
    if(pageName === ''){
      pageName = 'home';
    }else if(
        pageName === 'dashboard' && this.state.currentUser && this.state.isVisitor
        || pageName === 'dashboard' && !this.state.currentUser
      ){
      pageName += ' isVisitor';
    }
    if(currentPath.split('/')[1] === 'dashboard' || currentPath.split('/')[1] === 'admin'){
      return (
        <Col xs={12} md={12}
          style={ !this.state.currentUser
            ? {
              height:'100%',
              paddingRight:0,
              paddingLeft:0
              // backgroundColor: 'transparent',
              // backgroundImage:'url("assets/images/acessonegado.jpg")',
              // backgroundSize: 'cover'
              }
            : {
              padding:0,
              height:'100%'
              }
            }>
          <NavHeaderDashboard
            logoURL={this.state.logoURL}
            isMobile={this.state.isMobile}
            isVisitor={this.state.isVisitor}
            currentUser={this.state.currentUser}
            currentMenu={currentPath}
            pages={this.state.pages}
            products={this.state.products}
            categories={this.state.categories}
            packages={this.state.packages} />
          <NavHeaderDashboard
            logoURL={this.state.logoURL}
            isMobile={this.state.isMobile}
            isVisitor={this.state.isVisitor}
            currentUser={this.state.currentUser}
            sidebar
            pages={this.state.pages}
            products={this.state.products}
            categories={this.state.categories}
            packages={this.state.packages} />
          <Col md={this.state.currentUser && this.state.currentUser.email !== APP_CONFIG.MAIL_AUTH_VISITOR ? 10 : 12} xs={12} className={'contentContainer '+pageName}>

              {React.cloneElement(this.props.children, {
                logoURL: this.state.logoURL,
                isVisitor: this.state.isVisitor,
                currentUser: this.state.currentUser,
                packages: this.state.packages
              })}

          </Col>
          {/* <Footer /> */}
        </Col>
      );
    }else{
      // console.log('this.state.packages -> ',this.state.packages);
      let renderTopbarHeader = () => {
        if(this.state.packages.SHOP.topbarHeader){
          return (
            <Col md={12} className={'topbarHeader'}>
              <Col xs={12} md={6} className={'socialNetwork text-left'}>
                <ul>
                  {/*<li><Link to={'/'} className={'fa fa-phone'}>(16) 3721-4700</Link></li>

                  <li><a onClick={(e) => {e.preventDefault(); window.open(e.target.href);}} href={'https://instagram.com/ultragrip'} target={'_blank'} className={'fa fa-shopping-cart'}><i></i></a></li>
                  <li><Link to={'/login'} className={'fa fa-user'}><i></i></Link></li>
                  <li><Link to={'/logout'} className={'fa fa-sign-out'} style={{paddingTop:'3px',fontSize:12}} onClick={this.signOut}> <span>Sair</span></Link></li>
                  <li style={{color:'#FFF'}}>|</li>
                  <li><Link to={'/atendimento'} className={'fa fa-phone'}><i></i></Link></li>
                  <li><a onClick={(e) => {e.preventDefault(); window.open(e.target.href);}} href={'https://www.facebook.com/ultragrip/'} target={'_blank'} className={'fa fa-facebook'}><i></i></a></li>
                  <li><a onClick={(e) => {e.preventDefault(); window.open(e.target.href);}} href={'https://instagram.com/ultragrip'} target={'_blank'} className={'fa fa-instagram'}><i></i></a></li>*/}
                </ul>
              </Col>
            </Col>
          );
        }
      }
      return (
        <Col xs={12} md={12} className={'shop'}>
          <meta property="og:url"           content="https://graficavolare.devsuccess.com.br/" />
          <meta property="og:type"          content="website" />
          <meta property="og:title"         content="Gráfica Volare | Página Inicial" />
          <meta property="og:description"   content="Gráfica Rápida Digital" />
          <meta property="og:image"         content="https://graficavolare.devsuccess.com.br/assets/images/logo_volare.png" />
          { renderTopbarHeader() }
          <NavHeader
            logoURL={this.state.logoURL}
            isMobile={this.state.isMobile}
            isVisitor={this.state.isVisitor}
            currentUser={this.state.currentUser}
            currentMenu={currentPath}
            products={this.state.products}
            pages={this.state.pages}
            categories={this.state.categories}
            packages={this.state.packages}
            socialnetwork
            withSearch
            withCart
          />
          { pageName && pageName === 'home' ?
            <Carousel
                indicators={false}
                controls={true}
                interval={3000}
                direction={'prev'}>
              <Carousel.Item animateIn={true} animateOut={true} direction={'prev'}>
                <Parallax bgImage={'assets/images/slide_1.jpg'} strength={0} />
              </Carousel.Item>
            </Carousel>
          :
            ''
          }
          <Col md={12} className={'contentContainer '+pageName}>
              {React.cloneElement(this.props.children, {
                logoURL: this.state.logoURL,
                isVisitor: this.state.isVisitor,
                isMobile: this.state.isMobile,
                currentUser: this.state.currentUser,
                products: this.state.products,
                pages: this.state.pages,
                categories: this.state.categories,
                packages: this.state.packages
              })}
          </Col>
          <Footer pages={this.state.pages} />
        </Col>
      );
    }
  };//renderApp();

  render() {
    if(this.state.packages){
      return this.renderApp()
    }
  }
}
