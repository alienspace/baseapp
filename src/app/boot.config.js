export const APP_CONFIG = {
  PROJECT_VERSION: 0.5,
  PROJECT_NAME: 'Gráfica Volare',
  PROJECT_SLOGAN: 'Gráfica Rápida Digital',
  PROJECT_DOMAIN: 'graficavolare.com.br',
  PROJECT_PACKAGES: {
    DASHBOARD: {
      appManager: false,
      category: true,
      footer: true,
      logo: true,
      navHeader: true,
      page: true,
      product: true,
      settings: true
    },
    SHOP: {
      cart: true,
      cartBox: true,
      footer: true,
      loginBox: true,
      productGrid: true,
      searchBox: true,
      socialnetwork: true,
      topbarHeader: true,
      navFull: true
    },
    SITE: {
      footer: true,
      logo: true,
      navHeader: true,
      navFull: true
    },
  },
  MAIL_AUTH_VISITOR: 'visitante@graficavolare.com.br',
  PATH_BASE: location.hostname
}