import React from 'react';
import { render } from 'react-dom';
import { Router, Route, IndexRoute, browserHistory } from 'react-router';

/** CUSTOM COMPONENTS **/
import { App } from './App';
// import { APP_CONFIG } from './boot.config';
import { SetupContainer } from './containers/Global/SetupContainer.js';

/*
 * DASHBOARD COMPONENTS *
 */
import { Dashboard_AuthContainer } from './containers/Dashboard/Dashboard_AuthContainer';
// import { Dashboard_AppsContainer } from './containers/Dashboard/Dashboard_AppsContainer';
import { Dashboard_ProductsContainer } from './containers/Dashboard/Dashboard_ProductsContainer';
import { Dashboard_PagesContainer } from './containers/Dashboard/Dashboard_PagesContainer';
import { Dashboard_CategoriesContainer } from './containers/Dashboard/Dashboard_CategoriesContainer';
import { Dashboard_SettingsContainer } from './containers/Dashboard/Dashboard_SettingsContainer';
import { NotFoundContainer } from './containers/NotFoundContainer';

/*
 * SHOP COMPONENTS *
 */
import { Shop_HomeContainer } from './containers/Shop/Shop_HomeContainer';
import { Shop_ProductDetailsContainer } from './containers/Shop/Shop_ProductDetailsContainer';
import { Shop_CartContainer } from './containers/Shop/Shop_CartContainer';
import { Shop_LoginContainer } from './containers/Shop/Shop_LoginContainer';

/*
 * SITE COMPONENTS *
 */
import { Site_HomeContainer } from './containers/Site/Site_HomeContainer';
import { Site_AboutContainer } from './containers/Site/Site_AboutContainer';
import { Site_BlogContainer } from './containers/Site/Site_BlogContainer';
import { Site_ContactContainer } from './containers/Site/Site_ContactContainer';
// import { Site_BlogPostSingleContainer } from './containers/Site/Site_BlogPostSingleContainer';

render(
  <Router
    history={browserHistory}
    onUpdate={() => {
      let currentURL = browserHistory.getCurrentLocation();
      if(!currentURL.query.action && currentURL.query.action !== 'updateCart'){
        window.scrollTo(0, 0);
      }
      // console.log('currentURL -> ',currentURL);
    }}
    >
    <Route path="/" component={App}>
      <IndexRoute component={Shop_HomeContainer} />
      <Route path="/home" component={Shop_HomeContainer} />
      <Route path="/a-empresa" component={Site_AboutContainer} />
      <Route path="/blog" component={Site_BlogContainer}>
        <Route path="/blog/:post_slug" component={Site_BlogContainer} />
      </Route>
      <Route path="/product" component={Shop_ProductDetailsContainer}>
        <Route path="/product/:product_slug" component={Shop_ProductDetailsContainer} />
      </Route>
      <Route path="/contato" component={Site_ContactContainer} />

      <Route path="/carrinho" component={Shop_CartContainer} />
      <Route path="/login" component={Shop_LoginContainer} />

      <Route path="/dashboard" component={Dashboard_AuthContainer} />
      <Route path="/dashboard/profile" component={Dashboard_SettingsContainer} />
      {/*<Route path="/dashboard/apps" component={Dashboard_AppsContainer}>
        <Route path="/dashboard/apps/:action" component={Dashboard_AppsContainer} />
        <Route path="/dashboard/apps/:action/(:appID)" component={Dashboard_AppsContainer} />
      </Route>*/}
      <Route path="/dashboard/products" component={Dashboard_ProductsContainer}>
        <Route path="/dashboard/products/:action" component={Dashboard_ProductsContainer} />
        <Route path="/dashboard/products/:action/(:productID)" component={Dashboard_ProductsContainer} />
      </Route>
      <Route path="/dashboard/pages" component={Dashboard_PagesContainer}>
        <Route path="/dashboard/pages/:action" component={Dashboard_PagesContainer} />
        <Route path="/dashboard/pages/:action/(:pageID)" component={Dashboard_PagesContainer} />
      </Route>
      <Route path="/dashboard/categories" component={Dashboard_CategoriesContainer}>
        <Route path="/dashboard/categories/:action" component={Dashboard_CategoriesContainer} />
        <Route path="/dashboard/categories/:action/(:categoryID)" component={Dashboard_CategoriesContainer} />
      </Route>
      <Route path="/dashboard/settings" component={Dashboard_SettingsContainer} />
      <Route path="/dashboard/install" component={SetupContainer} />
      <Route path="/dashboard/*" component={NotFoundContainer} />
      <Route path="*" component={NotFoundContainer} />
    </Route>
  </Router>,
  document.querySelector('[data-js="root"]')
);
