#alienspace/projects
#dashspace

GIT CLONE
- Install modules/components

## Components
#### GLOBAL
- [AppManager](https://bitbucket.org/alienspace/globalappmanager)
- [AuthForm](https://bitbucket.org/alienspace/globalauthform)
- [Logo](https://bitbucket.org/alienspace/globallogo)
- [NavHeader](https://bitbucket.org/alienspace/globalnavheader)
- [PanelContent](https://bitbucket.org/alienspace/globalpanelcontent)

#### DASHBOARD
- [AppManager](https://bitbucket.org/alienspace/dashboardappmanager)
- [Category](https://bitbucket.org/alienspace/dashboardcategory)
- [Footer](dashboardFooter)
- [Logo](https://bitbucket.org/alienspace/dashboardlogo)
- [NavHeader](https://bitbucket.org/alienspace/dashboardnavheader)
- [Page](https://bitbucket.org/alienspace/dashboardpage)
- [Product](https://bitbucket.org/alienspace/dashboardproduct)
- [Settings](https://bitbucket.org/alienspace/dashboardsettings)

#### SHOP
- [Cart](https://bitbucket.org/alienspace/shopcart)
- [CartBox](https://bitbucket.org/alienspace/shopcartbox)
- [Footer](https://bitbucket.org/alienspace/shopfooter)
- [LoginBox](https://bitbucket.org/alienspace/shoploginbox)
- [ProductGrid](https://bitbucket.org/alienspace/shopproductgrid)

#### SITE
- [Footer](https://bitbucket.org/alienspace/sitefooter)
