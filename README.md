#### Folder Structure Default
###### - [App](https://bitbucket.org/alienspace/baseapp/src/3ea97fb6927ac3ba338a92aa6b496f1a7d78a0ee/src/app/?at=master)
######## - [Components](https://bitbucket.org/alienspace/baseapp/src/3ea97fb6927ac3ba338a92aa6b496f1a7d78a0ee/src/app/components/?at=master)

=====================================
########## - [Dashboard](https://bitbucket.org/alienspace/baseapp/src/3ea97fb6927ac3ba338a92aa6b496f1a7d78a0ee/src/app/components/Dashboard/?at=master)

=====================================
########## - [Global](https://bitbucket.org/alienspace/baseapp/src/3ea97fb6927ac3ba338a92aa6b496f1a7d78a0ee/src/app/components/Global/?at=master)
############## - [Footer](https://bitbucket.org/alienspace/baseapp/src/3ea97fb6927ac3ba338a92aa6b496f1a7d78a0ee/src/app/components/Global/Footer/?at=master)
############## - [Form](https://bitbucket.org/alienspace/baseapp/src/3ea97fb6927ac3ba338a92aa6b496f1a7d78a0ee/src/app/components/Global/Form/?at=master)
############## - [Forms](https://bitbucket.org/alienspace/baseapp/src/3ea97fb6927ac3ba338a92aa6b496f1a7d78a0ee/src/app/components/Global/Forms/?at=master)

=====================================
########## -  [Shop](https://bitbucket.org/alienspace/baseapp/src/3ea97fb6927ac3ba338a92aa6b496f1a7d78a0ee/src/app/components/Shop/?at=master)

=====================================
########## -
[Site](https://bitbucket.org/alienspace/baseapp/src/3ea97fb6927ac3ba338a92aa6b496f1a7d78a0ee/src/app/components/Site/?at=master)

######## - [Containers](https://bitbucket.org/alienspace/baseapp/src/3ea97fb6927ac3ba338a92aa6b496f1a7d78a0ee/src/app/containers/?at=master)
######## - [Util](https://bitbucket.org/alienspace/baseapp/src/3ea97fb6927ac3ba338a92aa6b496f1a7d78a0ee/src/app/util/?at=master)
########## - [nextStorage](https://bitbucket.org/alienspace/baseapp/src/3ea97fb6927ac3ba338a92aa6b496f1a7d78a0ee/src/app/util/nextStorage.js?at=master)
- get();
- getJSON();
- set();
- update();
- delete();
- removeItemCART();
[TOOLS](https://bitbucket.org/alienspace/baseapp/src/3ea97fb6927ac3ba338a92aa6b496f1a7d78a0ee/src/app/util/tools.global.js?at=master&fileviewer=file-view-default)
- uniqueID();
- convertInnerHTML();
- hasDecimalPart();
- getDecimalSymbol();
- filterNumbers();
- filterNumbersDotsAndCommas();
- getMoney();
- formatReal();
- stripAllTags();
- requiredInput();
- orderByCustom();
- removeArrayItem();
- insertArrayItem();
- loginVisitor();


########## -  APP_CONFIG ([APP_CONFIG](https://bitbucket.org/alienspace/baseapp/src/3ea97fb6927ac3ba338a92aa6b496f1a7d78a0ee/src/app/boot.config.js?at=master&fileviewer=file-view-default))

" ##########################################
export const APP_CONFIG = {
  PROJECT_VERSION: 0.5.0,
  PROJECT_NAME: 'Nome da Empresa',
  PROJECT_SLOGAN: 'Slogan da Empresa',
  PROJECT_DOMAIN: 'site.com.br',
  MAIL_AUTH_VISITOR: 'visitante@site.com.br',
  PATH_BASE: location.hostname
}
" ##########################################
- [Routes](https://bitbucket.org/alienspace/baseapp/src/3ea97fb6927ac3ba338a92aa6b496f1a7d78a0ee/src/app/boot.js?at=master&fileviewer=file-view-default)

- [Sending Feedback](#sending-feedback)
- [Folder Structure](#folder-structure)
  - [npm start](#npm-start)
  - [npm test](#npm-test)
  - [npm run build](#npm-run-build)
